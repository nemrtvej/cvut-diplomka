# DVBGrabber 2

## Directories:

- backend: PHP based backend application written in Symfony
- db: Directory containing database init script
- docker-configs: Configuration overrides for included docker containers
- frontend: contains HTML single-page-application for management
  - public: contains every file that should be displayed to end-user
- tv-guide-updater: Java utility for updating TV Schedule in database

## How to run it all:

1) Make sure you have Docker installed.
2) `docker-compose up`
3) Create your own user:
```
cd backend
console fos:user:create
```


Adminer instance is running on http://localhost:8008
Backend runs on http://localhost:8001
Frontend is on http://localhost:8080
PostgreSQL database is running on port 5432
RabbitMQ management is running on http://localhost:15672/ (guest:guest)

## Cron setup
On the host machine, cron has to contain following entries:
 
```
* * * * * marek docker exec -i dvbgrabber__php php app/console rabbitmq:consumer -m 5 recording_status
```

## Troubleshooting:


### Q: I get message like this during rebuilding Docker container:

```
WindowsError: [Error 3] The system cannot find the path specified: u'c:\\Hephaestos\\Development\\DVBGrabber\\backend\\app\\cache\\dev\\annotations\\2c\\5b53796d666f6e795c42756e646c655c4672616d65776f726b42756e646c655c436f6e74726f6c6c65725c436f6e74726f6c6c6572236372656174654e6f74466f756e64457863657074696f6e405b416e6e6f745d5d5b315d.doctrinecache.data'
Failed to execute script docker-compose
```

A: Completely purge directory backend/app/cache/*

### Q: While running composer or console commands in the directory backend, error occures
  
A: There might be two problems:

1) Scripting expects, that there there is `./bin` in your PATH variable
2) Init scripts are currently written for Windows (e.g. those are batch scripts, not bash scripts).
   However, they are really simple for now, so changing it for Linux should not be a problem.
   
   
   
Sources: http://stackoverflow.com/questions/26822067/running-cron-python-jobs-within-docker


Fakestream
:sout=#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100}:http{mux=ffmpeg{mux=flv},dst=:8080/video} :sout-keep