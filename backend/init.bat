@echo off

set container_php=dvbgrabber__php

mkdir vendor bin schedule
docker exec -it %container_php% composer install

call :heredoc install_updateSchedule >bin/updateSchedule.bat && goto updateSchedule_installed
    wget http://televize.sh.cvut.cz/xmltv/all.xml -O schedule/all.xml
    docker exec -it !container_php! php app/console dvb:schedule:import schedule/all.xml
:updateSchedule_installed

call :heredoc install_composer >bin/composer.bat && goto composer_installed
    docker exec -it !container_php! composer %*
:composer_installed

call :heredoc install_console >bin/console.bat && goto console_installed
    docker exec -it !container_php! php app/console %*
:console_installed

goto :EOF


:: ########################################
:: ## Here's the heredoc processing code ##
:: ########################################
:heredoc <uniqueIDX>
setlocal enabledelayedexpansion
set go=
for /f "delims=" %%A in ('findstr /n "^" "%~f0"') do (
    set "line=%%A" && set "line=!line:*:=!"
    if defined go (if #!line:~1!==#!go::=! (goto :EOF) else echo(!line!)
    if "!line:~0,13!"=="call :heredoc" (
        for /f "tokens=3 delims=>^ " %%i in ("!line!") do (
            if #%%i==#%1 (
                for /f "tokens=2 delims=&" %%I in ("!line!") do (
                    for /f "tokens=2" %%x in ("%%I") do set "go=%%x"
                )
            )
        )
    )
)
goto :EOF