<?php

namespace AppBundle\Command;

use AppBundle\Model\ScheduledRecordModel;
use AppBundle\Model\ShowModel;
use AppBundle\Model\TvSchedule\Import\ScheduleImporter;
use AppBundle\Service\RabbitFacade;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ForceRemoveCommand extends Command
{
    /**
     * @var RabbitFacade
     */
    private $rabbitFacade;

    /**
     * @var ScheduledRecordModel
     */
    private $scheduledRecordModel;

    /**
     * @var ShowModel
     */
    private $showModel;

    public function __construct(
        RabbitFacade $rabbitFacade,
        ScheduledRecordModel $scheduledRecordModel,
        ShowModel $showModel
    ) {
        parent::__construct();

        $this->rabbitFacade = $rabbitFacade;
        $this->scheduledRecordModel = $scheduledRecordModel;
        $this->showModel = $showModel;
    }

    protected function configure()
    {
        $this
            ->setName('dvb:force:remove')
            ->setDescription('Force removal of recorded show')
            ->addArgument('showId', InputArgument::REQUIRED, 'ID of show to be removed');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $showId = $input->getArgument('showId');
        $show = $this->showModel->findById($showId);
        if (is_null($show)) {
            $output->writeln(sprintf('There is no show with id %s', $showId));
            return 1;
        }


        $scheduledRecord = $this->scheduledRecordModel->find($show->getId());
        if (is_null($scheduledRecord)) {
            $output->writeln(sprintf('There is no scheduled recording for show %s', $showId));
            return 1;
        }

        $output->writeln(sprintf('Scheduled removal for show %s (%s)', $show->getId(), $show->getTitle()));

        $this->rabbitFacade->sendRemoveRecordedRecordingMessage($scheduledRecord);

        return 0;
    }
}