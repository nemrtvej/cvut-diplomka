<?php

namespace AppBundle\Command;

use AppBundle\Model\TvSchedule\Import\ScheduleImporter;
use AppBundle\Model\TvSchedule\Import\ScheduleImportException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class ImportTvScheduleCommand extends Command
{

    /**
     * @var ScheduleImporter
     */
    private $scheduleImporter;

    public function __construct(ScheduleImporter $scheduleImporter)
    {
        parent::__construct();

        $this->scheduleImporter = $scheduleImporter;
    }

    protected function configure()
    {
        $this
            ->setName('dvb:schedule:import')
            ->setDescription('Import new TV schedule data from given XmlTv file.')
            ->addArgument('file', InputArgument::REQUIRED, 'Source XmlTv data file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('file');
        $fileSystem = new Filesystem();

        if (!$fileSystem->exists($filePath)) {
            $output->writeln(sprintf('File "%s" does not exists', $filePath));
            return 1;
        }

        try {
            $file = new File($filePath);
            $this->scheduleImporter->importXmlTv($file, $output);
        } catch (ScheduleImportException $e) {
            $output->writeln(sprintf('Error occured during file "%s" importing.', $filePath));
            $output->writeln(sprintf('%s', $e->getMessage()));
            return 2;
        }

        $output->writeln('Import was successfull.');

        return 0;
    }
}