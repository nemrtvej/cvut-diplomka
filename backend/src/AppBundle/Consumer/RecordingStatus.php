<?php

namespace AppBundle\Consumer;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Enum\ScheduledRecordStatesEnum;
use AppBundle\Model\ScheduledRecordModel;
use AppBundle\Service\RabbitFacade;
use AppBundle\Service\ShowRecordedNotificator;
use Doctrine\ORM\EntityManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class RecordingStatus implements ConsumerInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RabbitFacade
     */
    private $rabbitFacade;

    /**
     * @var ScheduledRecordModel
     */
    private $scheduledRecordModel;

    /**
     * @var ShowRecordedNotificator
     */
    private $showRecordedNotificator;

    /**
     * RecordingStatus constructor.
     *
     * @param EntityManager           $entityManager
     * @param LoggerInterface         $logger
     * @param RabbitFacade            $rabbitFacade
     * @param ScheduledRecordModel    $scheduledRecordModel
     * @param ShowRecordedNotificator $showRecordedNotificator
     */
    public function __construct(
        EntityManager $entityManager,
        LoggerInterface $logger,
        RabbitFacade $rabbitFacade,
        ScheduledRecordModel $scheduledRecordModel,
        ShowRecordedNotificator $showRecordedNotificator
    )
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->rabbitFacade = $rabbitFacade;
        $this->scheduledRecordModel = $scheduledRecordModel;
        $this->showRecordedNotificator = $showRecordedNotificator;
    }

    /**
     * @param AMQPMessage $msg The message
     *
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $body = $msg->getBody();

        $data = json_decode($body, true);

        if (is_null($data)) {
            $this->logger->info(sprintf('Unparseable data obtained: %s', $body));
            // some garbage, skip and continue..
            return true;
        }

        $id = $data['id'];
        /** @var ScheduledRecord $entity */
        $entity = $this->scheduledRecordModel->find($id);
        if (is_null($entity)) {
            $this->logger->info(sprintf('Unkown ScheduledRecording ID obtained: %s', $id));
            // some unknown scheduledRecord... skip and continue
            return true;
        }

        $status = $data['status'];
        if (!in_array($status, ScheduledRecordStatesEnum::getEnum())) {
            $this->logger->info(sprintf('Unkown ScheduledRecording state obtained: %s', $status));
            return true;
        }
        $entity->setState($status);

        if (array_key_exists('size', $data)) {
            $entity->setSize($data['size']);
        }

        if (array_key_exists('time_processed', $data)) {
            $time = new \DateTime();
            $time->setTimestamp((int)$data['time_processed']);
            $entity->setTimeProcessed($time);
        }

        if (array_key_exists('length', $data)) {
            $entity->setLength((int)$data['length']);
        }

        $this->entityManager->flush($entity);

        if ($status === ScheduledRecordStatesEnum::RECORDED) {
            $this->rabbitFacade->sendTranscodeMessage($entity);
        } else if ($status === ScheduledRecordStatesEnum::PROCESSED) {
            $this->showRecordedNotificator->sendShowRecordedMails($entity);
        }

        return true;
    }
}