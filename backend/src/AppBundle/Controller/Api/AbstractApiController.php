<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\AbstractBaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractBaseController
{
    protected function createJsonResponse($data, $responseCode = Response::HTTP_OK)
    {
        return new JsonResponse($data, $responseCode);
    }


    protected function createErrorResponse(\Exception $e, $message)
    {
        return $this->createJsonResponse(
            [
                'status' => 'FAIL',
                'message' => $message,
            ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}