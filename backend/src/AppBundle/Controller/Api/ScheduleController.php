<?php

namespace AppBundle\Controller\Api;

use AppBundle\Model\ChannelModel;
use AppBundle\Model\ShowModel;
use AppBundle\Model\TvSchedule\Export\ScheduleExporter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class ScheduleController
 *
 * @package AppBundle\Controller\Api
 *
 * @Sensio\Route("/schedule", service="app.api.schedule_controller")
 */
class ScheduleController extends AbstractApiController
{
    /**
     * @var ChannelModel
     */
    private $channelModel;

    /**
     * @var ShowModel
     */
    private $showModel;

    /**
     * @var ScheduleExporter
     */
    private $scheduleExporter;

    /**
     * ScheduleController constructor.
     *
     * @param ChannelModel     $channelModel
     * @param ShowModel        $showModel
     * @param ScheduleExporter $scheduleExporter
     */
    public function __construct(ChannelModel $channelModel, ShowModel $showModel, ScheduleExporter $scheduleExporter)
    {
        $this->channelModel = $channelModel;
        $this->showModel = $showModel;
        $this->scheduleExporter = $scheduleExporter;
    }

    /**
     * @Sensio\Route("/full", name="api_schedule_full")
     */
    public function fullAction()
    {
        $start = new \DateTimeImmutable();
        $stop = $start->modify('+7 day');

        $data = $this->scheduleExporter->getData($start, $stop);

        return $this->createJsonResponse($data);
    }
}