<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Entity\Show;
use AppBundle\Entity\User;
use AppBundle\Entity\UserScheduledRecord;
use AppBundle\Enum\ScheduledRecordStatesEnum;
use AppBundle\Model\Exception\ShowException;
use AppBundle\Model\Exception\UserScheduledRecordException;
use AppBundle\Model\UserScheduledRecordModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 *
 * @Sensio\Route("/show", service="app.api.show_controller")
 */
class ShowController extends AbstractApiController
{
    /**
     * @var UserScheduledRecordModel
     */
    private $userScheduledRecordModel;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * ShowController constructor.
     *
     * @param UserScheduledRecordModel $userScheduledRecordModel
     * @param TokenStorageInterface   $tokenStorage
     */
    public function __construct(UserScheduledRecordModel $userScheduledRecordModel, TokenStorageInterface $tokenStorage)
    {
        $this->userScheduledRecordModel = $userScheduledRecordModel;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Sensio\Method({"POST"})
     * @Sensio\Route("/scheduled/{id}", name="api_show_schedule_record")
     * @Sensio\ParamConverter("show")
     * @param Show $show
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \AppBundle\Model\Exception\ScheduledRecordException
     * @throws \AppBundle\Model\Exception\UserScheduledRecordException
     */
    public function scheduleRecordAction(Show $show)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        try {
            $this->userScheduledRecordModel->scheduleRecordingForUser($show, $user);
        } catch (ShowException $e) {
            return $this->createErrorResponse($e, 'Nepodařilo se naplánovat nahrávání');
        }

        return $this->createJsonResponse(['status' => 'OK'], Response::HTTP_CREATED);
    }

    /**
     * @Sensio\Method({"DELETE"})
     * @Sensio\Route("/scheduled/{id}", name="api_show_unschedule_record")
     * @Sensio\ParamConverter("show")
     * @param Show $show
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \AppBundle\Model\Exception\ScheduledRecordException
     * @throws \AppBundle\Model\Exception\UserScheduledRecordException
     */
    public function unscheduleRecordAction(Show $show)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        try {
            $this->userScheduledRecordModel->unscheduleRecordingForUser($show, $user);
        } catch (ShowException $e) {
            return $this->createErrorResponse($e, 'Nepodařilo se zrušit nahrávání');
        }

        return $this->createJsonResponse(['status' => 'OK'], Response::HTTP_ACCEPTED);
    }

    /**
     * @Sensio\Method({"DELETE"})
     * @Sensio\Route("/recorded/{id}", name="api_show_remove_recorded_show")
     * @Sensio\ParamConverter("scheduledRecord")
     * @param User            $user
     * @param ScheduledRecord $scheduledRecord
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function removeRecordedShowAction(ScheduledRecord $scheduledRecord)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        try {
            $userScheduledRecord = $this->userScheduledRecordModel->findByScheduledRecordAndUser($scheduledRecord, $user);
            $this->userScheduledRecordModel->removeRecordedRecord($userScheduledRecord);
        } catch (UserScheduledRecordException $e) {
            return $this->createErrorResponse($e, 'Nepodařilo smazat nahrávku');
        }

        return $this->createJsonResponse(['status' => 'OK'], Response::HTTP_ACCEPTED);
    }


    /**
     * @Sensio\Method({"GET"})
     * @Sensio\Route("/{id}", name="api_show_download")
     * @Sensio\ParamConverter("scheduledRecord")
     * @param ScheduledRecord $scheduledRecord
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     */
    public function downloadShow(ScheduledRecord $scheduledRecord = null)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $show = $scheduledRecord->getShow();

        $userScheduledRecording = $this->userScheduledRecordModel->findByShowAndUser($show, $user);
        if (is_null($userScheduledRecording)) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        if ($userScheduledRecording->getScheduledRecord()->getState() === ScheduledRecordStatesEnum::PROCESSED) {
            $response = new BinaryFileResponse('/data/'.$show->getId().'/output.raw');
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'DVB-'.$show->getTitle() . '.avi');

        } else {
            $response = new Response('', Response::HTTP_FORBIDDEN);
        }

        return $response;
    }

}