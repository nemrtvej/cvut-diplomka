<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\Request;

class TestController extends AbstractApiController
{
    /**
     * @Sensio\Route("/", name="api_test")
     */
    public function indexAction(Request $request)
    {
        dump($this->getUser()); die();
    }
}