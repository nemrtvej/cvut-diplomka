<?php

namespace AppBundle\Controller\Api;

use AppBundle\Model\ScheduledRecordModel;
use AppBundle\Service\Serializer\ScheduledRecordSerializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 *
 * @Sensio\Route("/user", service="app.api.user_controller")
 */
class UserController extends AbstractApiController
{
    /**
     * @var ScheduledRecordModel
     */
    private $scheduledRecordModel;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ScheduledRecordSerializer
     */
    private $scheduledRecordSerializer;

    /**
     *
     * @param ScheduledRecordModel      $scheduledRecordModel
     * @param ScheduledRecordSerializer $scheduledRecordSerializer
     * @param TokenStorageInterface     $tokenStorage
     */
    public function __construct(
        ScheduledRecordModel $scheduledRecordModel,
        ScheduledRecordSerializer $scheduledRecordSerializer,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->scheduledRecordModel = $scheduledRecordModel;
        $this->scheduledRecordSerializer = $scheduledRecordSerializer;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @Sensio\Method({"GET"})
     * @Sensio\Route("/scheduled", name="api_user_list_scheduled_shows_for_user")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listScheduledShowsForUserAction()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $scheduledRecords = $this->scheduledRecordModel->getScheduledRecordsForUser($user);
        $serializedStructure = $this->scheduledRecordSerializer->serializeScheduledCollection($scheduledRecords);

        return $this->createJsonResponse($serializedStructure->toArray());
    }


    /**
     * @Sensio\Method({"GET"})
     * @Sensio\Route("/recorded", name="api_user_list_recorded_shows_for_user")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listRecordedShowsForUserAction()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $scheduledRecords = $this->scheduledRecordModel->getRecordedRecordsForUser($user);
        $serializedStructure = $this->scheduledRecordSerializer->serializeRecordedCollection($scheduledRecords);

        return $this->createJsonResponse($serializedStructure->toArray());
    }
}