<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChannelRepository")
 * @ORM\Table(name="channels")
 *
 * Class Channel
 * @package AppBundle\Entity
 */
class Channel
{
    use IdTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="dvb_source_url", type="string", length=250, nullable=true)
     */
    private $dvbSourceUrl;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="xml_id", type="string", length=50, nullable=false)
     */
    private $xmlId;

    /**
     * @var Collection|Show[]
     * @ORM\OneToMany(targetEntity="Show", mappedBy="channel")
     */
    private $shows;

    /**
     * Channel constructor.
     */
    public function __construct()
    {
        $this->shows = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getDvbSourceUrl()
    {
        return $this->dvbSourceUrl;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @return Show[]|Collection
     */
    public function getShows()
    {
        return $this->shows;
    }

    /**
     * @param string $dvbSourceUrl
     */
    public function setDvbSourceUrl($dvbSourceUrl)
    {
        $this->dvbSourceUrl = $dvbSourceUrl;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $xmlId
     */
    public function setXmlId($xmlId)
    {
        $this->xmlId = $xmlId;
    }
}