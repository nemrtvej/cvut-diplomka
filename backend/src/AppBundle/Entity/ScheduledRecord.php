<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\IdTrait;
use AppBundle\Enum\ScheduledRecordStatesEnum;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ScheduledRecordRepository")
 * @ORM\Table(name="scheduled_records")
 *
 * Class ScheduledRecord
 * @package AppBundle\Entity
 */
class ScheduledRecord
{
    use IdTrait;

    /**
     * @var Show
     * @ORM\OneToOne(targetEntity="Show")
     */
    private $show;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=20, nullable=false)
     */
    private $state;

    /**
     * @var Collection|UserScheduledRecord[]
     * @ORM\OneToMany(targetEntity="UserScheduledRecord", mappedBy="scheduledRecord")
     */
    private $userScheduledRecords;

    /**
     * @var integer
     * @ORM\Column(name="size", type="bigint", nullable=true)
     */
    private $size;


    /**
     * @var integer
     * @ORM\Column(name="length", type="integer", nullable=true)
     */
    private $length;

    /**
     * @var \DateTimeInterface|null
     * @ORM\Column(name="time_processed", type="datetime", nullable=true)
     */
    private $timeProcessed;

    /**
     * ScheduledRecord constructor.
     * @param Show $show
     */
    public function __construct(Show $show)
    {
        $this->show = $show;
        $this->state = ScheduledRecordStatesEnum::SCHEDULED;
    }

    /**
     * @return Show
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

    /**
     * @return UserScheduledRecord[]|Collection
     */
    public function getUserScheduledRecords()
    {
        return $this->userScheduledRecords;
    }

    /**
     * @param UserScheduledRecord $userScheduledRecord
     */
    public function removeUserRecording(UserScheduledRecord $userScheduledRecord)
    {
        if ($this->userScheduledRecords->contains($userScheduledRecord)) {
            $this->userScheduledRecords->removeElement($userScheduledRecord);
        }
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTimeProcessed()
    {
        return $this->timeProcessed;
    }

    /**
     * @param \DateTimeInterface|null $timeProcessed
     */
    public function setTimeProcessed($timeProcessed)
    {
        $this->timeProcessed = $timeProcessed;
    }

    /**
     * @return int
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }
}