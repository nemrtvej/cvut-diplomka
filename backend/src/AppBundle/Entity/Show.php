<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\IdTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShowRepository")
 * @ORM\Table(name="shows")
 *
 * Class Show
 * @package AppBundle\Entity
 */
class Show
{
    use IdTrait;

    /**
     * @var DateTime
     * @ORM\Column(name="start", type="datetime", nullable=false)
     */
    private $start;

    /**
     * @var DateTime
     * @ORM\Column(name="stop", type="datetime", nullable=false)
     */
    private $stop;

    /**
     * @var Channel
     * @ORM\ManyToOne(targetEntity="Channel", inversedBy="shows")
     */
    private $channel;

    /**
     * @var string
     * @ORM\Column(name="title", type="text", nullable=false)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="sub_title", type="text", nullable=false)
     */
    private $subTitle;

    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * Show constructor.
     * @param DateTime $start
     * @param DateTime $stop
     * @param Channel $channel
     * @param string $title
     * @param string $subTitle
     * @param string $description
     */
    public function __construct(DateTime $start, DateTime $stop, Channel $channel, $title, $subTitle, $description)
    {
        $this->start = $start;
        $this->stop = $stop;
        $this->channel = $channel;
        $this->title = $title;
        $this->subTitle = $subTitle;
        $this->description = $description;
    }

    /**
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @return DateTime
     */
    public function getStop()
    {
        return $this->stop;
    }

    /**
     * @return Channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return sprintf(
            '%s (%s): %s - %s',
            $this->getTitle(),
            $this->getChannel()->getName(),
            $this->getStart()->format('H:i:s d.m.Y'),
            $this->getStop()->format('H:i:s d.m.Y')
        );
    }
}