<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="UserScheduledRecord", mappedBy="user")
     * @var Collection|UserScheduledRecord[]
     */
    private $userScheduledRecords;

    public function __construct()
    {
        parent::__construct();
        // your own logic

        $this->userScheduledRecords = new ArrayCollection();
    }

    public function addUserScheduledRecord(UserScheduledRecord $userScheduledRecord)
    {
        if ($this->userScheduledRecords->contains($userScheduledRecord)) {
            return;
        }

        $this->userScheduledRecords->add($userScheduledRecord);
    }

    public function removeUserScheduledRecord(UserScheduledRecord $userScheduledRecord)
    {
        if (!$this->userScheduledRecords->contains($userScheduledRecord)) {
            return;
        }

        $this->userScheduledRecords->removeElement($userScheduledRecord);
    }
}