<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Traits\IdTrait;
use AppBundle\Enum\UserScheduledRecordStatesEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserScheduledRecordRepository")
 * @ORM\Table(name="user_scheduled_records")
 *
 * Class UserScheduledRecord
 * @package AppBundle\Entity
 */
class UserScheduledRecord
{
    use IdTrait;

    /**
     * @var ScheduledRecord
     * @ORM\ManyToOne(targetEntity="ScheduledRecord", inversedBy="userScheduledRecords")
     */
    private $scheduledRecord;

    /**
     * @var string
     * @ORM\Column(name="state", type="string", length=30, nullable=false)
     */
    private $state;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userScheduledRecords")
     */
    private $user;

    /**
     * UserScheduledRecord constructor.
     * @param ScheduledRecord $scheduledRecord
     * @param User $user
     */
    public function __construct(ScheduledRecord $scheduledRecord, User $user)
    {
        $this->scheduledRecord = $scheduledRecord;
        $this->user = $user;
        $this->state = UserScheduledRecordStatesEnum::PLANNED;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return ScheduledRecord
     */
    public function getScheduledRecord()
    {
        return $this->scheduledRecord;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}