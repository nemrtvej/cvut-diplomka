<?php

namespace AppBundle\Enum;


class ScheduledRecordStatesEnum
{
    const SCHEDULED = 'SCHEDULED';
    const RECORDING = 'RECORDING';
    const RECORDED = 'RECORDED';
    const TRANSCODING = 'TRANSCODING';
    const COPYING = 'COPYING';
    const PROCESSED = 'PROCESSED';
    const REMOVED = 'REMOVED';

    public static function getEnum()
    {
        return [
            self::SCHEDULED,
            self::RECORDING,
            self::RECORDED,
            self::TRANSCODING,
            self::COPYING,
            self::PROCESSED,
            self::REMOVED,
        ];
    }
}