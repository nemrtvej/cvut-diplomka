<?php

namespace AppBundle\Enum;

class UserScheduledRecordStatesEnum
{
    const PLANNED = 'PLANNNED';
    const DOWNLOADED = 'DOWNLOADED';
    const REMOVED = 'REMOVED';
}