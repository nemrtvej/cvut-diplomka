<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170313153546 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE channels_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE scheduled_records_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shows_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_scheduled_records_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE channels (id INT NOT NULL, dvb_source_url VARCHAR(250) DEFAULT NULL, name VARCHAR(100) NOT NULL, xml_id VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE scheduled_records (id INT NOT NULL, show_id INT DEFAULT NULL, state VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F85A0E02D0C1FC64 ON scheduled_records (show_id)');
        $this->addSql('CREATE TABLE shows (id INT NOT NULL, channel_id INT DEFAULT NULL, start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, stop TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, title TEXT NOT NULL, sub_title TEXT NOT NULL, description TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6C3BF14472F5A1AA ON shows (channel_id)');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, locked BOOLEAN NOT NULL, expired BOOLEAN NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, credentials_expired BOOLEAN NOT NULL, credentials_expire_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E992FC23A8 ON users (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9A0D96FBF ON users (email_canonical)');
        $this->addSql('COMMENT ON COLUMN users.roles IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE user_scheduled_records (id INT NOT NULL, scheduled_record_id INT DEFAULT NULL, user_id INT DEFAULT NULL, state VARCHAR(30) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_45B5295BE06F0B ON user_scheduled_records (scheduled_record_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45B529A76ED395 ON user_scheduled_records (user_id)');
        $this->addSql('ALTER TABLE scheduled_records ADD CONSTRAINT FK_F85A0E02D0C1FC64 FOREIGN KEY (show_id) REFERENCES shows (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shows ADD CONSTRAINT FK_6C3BF14472F5A1AA FOREIGN KEY (channel_id) REFERENCES channels (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_scheduled_records ADD CONSTRAINT FK_45B5295BE06F0B FOREIGN KEY (scheduled_record_id) REFERENCES scheduled_records (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_scheduled_records ADD CONSTRAINT FK_45B529A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shows DROP CONSTRAINT FK_6C3BF14472F5A1AA');
        $this->addSql('ALTER TABLE user_scheduled_records DROP CONSTRAINT FK_45B5295BE06F0B');
        $this->addSql('ALTER TABLE scheduled_records DROP CONSTRAINT FK_F85A0E02D0C1FC64');
        $this->addSql('ALTER TABLE user_scheduled_records DROP CONSTRAINT FK_45B529A76ED395');
        $this->addSql('DROP SEQUENCE channels_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE scheduled_records_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shows_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_scheduled_records_id_seq CASCADE');
        $this->addSql('DROP TABLE channels');
        $this->addSql('DROP TABLE scheduled_records');
        $this->addSql('DROP TABLE shows');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_scheduled_records');
    }
}