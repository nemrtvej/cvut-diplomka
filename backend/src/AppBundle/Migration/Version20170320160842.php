<?php

namespace AppBundle\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170320160842 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DROP INDEX UNIQ_45B529A76ED395');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45B529A76ED395 ON user_scheduled_records (user_id, scheduled_record_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP INDEX UNIQ_45B529A76ED395');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45B529A76ED395 ON user_scheduled_records (user_id)');
    }
}
