<?php

namespace AppBundle\Model;

use AppBundle\Entity\Channel;
use AppBundle\Repository\ChannelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ChannelModel extends AbstractModel
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * ChannelModel constructor.
     * @param ChannelRepository $channelRepository
     */
    public function __construct(ChannelRepository $channelRepository)
    {
        $this->channelRepository = $channelRepository;
    }

    /**
     * @return Collection|Channel[]
     */
    public function getListOfChannelForSynchronization()
    {
        return new ArrayCollection($this->channelRepository->getListOfChannelForSynchronization());
    }

    /**
     * @param string $xmlId
     * @param string $channelName
     * @param bool   $autoFlush
     *
     * @return Channel
     */
    public function createChannel($xmlId, $channelName, $autoFlush = true)
    {
        $channel = new Channel();
        $channel->setXmlId($xmlId);
        $channel->setName($channelName);

        $this->entityManager->persist($channel);
        $this->save($channel, $autoFlush);

        return $channel;
    }

    private function save($channel, $flush)
    {
        if ($flush) {
            $this->entityManager->flush();
        }
    }
}