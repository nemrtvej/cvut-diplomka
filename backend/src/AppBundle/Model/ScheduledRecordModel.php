<?php

namespace AppBundle\Model;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Entity\Show;
use AppBundle\Entity\User;
use AppBundle\Enum\ScheduledRecordStatesEnum;
use AppBundle\Model\Exception\ScheduledRecordException;
use AppBundle\Repository\ScheduledRecordRepository;
use AppBundle\Repository\UserScheduledRecordRepository;
use AppBundle\Service\RabbitFacade;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;

class ScheduledRecordModel extends AbstractModel
{

    /**
     * @var RabbitFacade
     */
    private $rabbitFacade;

    /**
     * @var ScheduledRecordRepository
     */
    private $scheduledRecordRepository;

    /**
     * @var UserScheduledRecordRepository
     */
    private $userScheduledRecordRepository;

    /**
     * ScheduledRecordModel constructor.
     *
     * @param RabbitFacade                  $rabbitFacade
     * @param ScheduledRecordRepository     $scheduledRecordRepository
     * @param UserScheduledRecordRepository $userScheduledRecordRepository
     */
    public function __construct(
        RabbitFacade $rabbitFacade,
        ScheduledRecordRepository $scheduledRecordRepository,
        UserScheduledRecordRepository $userScheduledRecordRepository
    )
    {
        $this->rabbitFacade = $rabbitFacade;
        $this->scheduledRecordRepository = $scheduledRecordRepository;
        $this->userScheduledRecordRepository = $userScheduledRecordRepository;
    }

    /**
     * @param Show $show
     *
     * @return ScheduledRecord
     * @throws ScheduledRecordException
     */
    public function getScheduledRecordForShow(Show $show)
    {
        try {
            $currentRecord = $this->scheduledRecordRepository->findOneByShow($show);

            if (!is_null($currentRecord)) {
                return $currentRecord;
            }

            $record = new ScheduledRecord($show);

            $this->entityManager->persist($record);
            $this->entityManager->flush($record);

            $this->rabbitFacade->sendScheduleRecordingMessage($record);

            return $record;
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            throw new ScheduledRecordException('Failed to register new recording', null, $e);
        }
    }

    public function getScheduledRecordsForUser(User $user)
    {
        $states = [
            ScheduledRecordStatesEnum::TRANSCODING,
            ScheduledRecordStatesEnum::SCHEDULED,
            ScheduledRecordStatesEnum::COPYING,
            ScheduledRecordStatesEnum::RECORDING,
            ScheduledRecordStatesEnum::RECORDED,
        ];

        return new ArrayCollection($this->scheduledRecordRepository->findRecordsInStatesForUser($states, $user));
    }

    public function getRecordedRecordsForUser($user)
    {
        $states = [ScheduledRecordStatesEnum::PROCESSED];

        return new ArrayCollection($this->scheduledRecordRepository->findRecordsInStatesForUser($states, $user));
    }

    public function checkRecordingForRemoval(ScheduledRecord $scheduledRecord)
    {
        try {
            if ($scheduledRecord->getUserScheduledRecords()->count() === 0 && $scheduledRecord->getState() === ScheduledRecordStatesEnum::SCHEDULED) {
                $this->entityManager->remove($scheduledRecord);
                $this->entityManager->flush($scheduledRecord);

                $this->rabbitFacade->sendRemoveScheduledRecordingMessage($scheduledRecord);
            }
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            throw new ScheduledRecordException('Failed to remove scheduled recording', null, $e);
        }
    }

    /**
     * @param ScheduledRecord $recordedRecord
     *
     * @throws ScheduledRecordException
     */
    public function checkProcessedRecordingForRemoval(ScheduledRecord $recordedRecord)
    {
        try {
            $unremovedUserRecordingsCount = $this->userScheduledRecordRepository->countUnremovedUserRecordings($recordedRecord);

            if ($unremovedUserRecordingsCount === 0 && $recordedRecord->getState() === ScheduledRecordStatesEnum::PROCESSED) {

                $recordedRecord->setState(ScheduledRecordStatesEnum::REMOVED);
                $this->entityManager->flush($recordedRecord);

                $this->rabbitFacade->sendRemoveRecordedRecordingMessage($recordedRecord);
            }
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            throw new ScheduledRecordException('Failed to remove processed recording', null, $e);
        }
    }

    public function find($id)
    {
        return $this->scheduledRecordRepository->findOneBy(['show' => $id]);
    }
}