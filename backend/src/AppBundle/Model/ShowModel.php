<?php

namespace AppBundle\Model;

use AppBundle\Entity\Channel;
use AppBundle\Entity\Show;
use AppBundle\Model\TvSchedule\Import\Container\Show as ShowContainer;
use AppBundle\Repository\ShowRepository;
use Doctrine\Common\Collections\ArrayCollection;

class ShowModel extends AbstractModel
{

    /**
     * @var ShowRepository
     */
    private $showRepository;

    /**
     * ShowModel constructor.
     *
     * @param ShowRepository $showRepository
     */
    public function __construct(ShowRepository $showRepository)
    {
        $this->showRepository = $showRepository;
    }

    public function findShowInSlot($channelXmlId, \DateTime $start, \DateTime $stop)
    {
        return new ArrayCollection($this->showRepository->findShowsInSlot($channelXmlId, $start, $stop));
    }

    public function createShowFromContainer(Channel $channel, ShowContainer $show, $autoFlush = true)
    {
        $showEntity = new Show(
            $show->getStart(),
            $show->getStop(),
            $channel,
            $show->getName(),
            $show->getSubTitle(),
            $show->getDesc()
        );

        $this->entityManager->persist($showEntity);

        $this->save($showEntity, $autoFlush);
    }

    private function save($show, $flush)
    {
        if ($flush) {
            $this->entityManager->flush();
        }
    }

    public function removeShow(Show $show)
    {
        // @todo: handle already scheduled recordings for shows, which will be cancelled

        $this->entityManager->remove($show);
        $this->entityManager->flush();
    }

    public function findShows($channelId, $showTitle, \DateTimeInterface $start, \DateTimeInterface $stop)
    {
        return $this->showRepository->findShows($channelId, $showTitle, $start, $stop);
    }

    /**
     * @param $showId
     * @return Show|null
     */
    public function findById($showId)
    {
        return $this->showRepository->find($showId);
    }
}