<?php

namespace AppBundle\Model\TvSchedule\Export;

use AppBundle\Repository\ShowRepository;
use DateTimeInterface;
use Doctrine\ORM\EntityManager;

class ScheduleExporter
{
    const DAY_OF_YEAR = 'z';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ShowRepository
     */
    private $showRepository;

    /**
     * ScheduleExporter constructor.
     *
     * @param EntityManager  $entityManager
     * @param ShowRepository $showRepository
     */
    public function __construct(EntityManager $entityManager, ShowRepository $showRepository)
    {
        $this->entityManager = $entityManager;
        $this->showRepository = $showRepository;
    }


    public function getData(DateTimeInterface $dateFrom, DateTimeInterface $dateTo)
    {
        $result = [];
        $rawData = $this->showRepository->findDataForExport($dateFrom, $dateTo);

        $previousChannelName = null;
        $previousStartTime = null;

        foreach ($rawData as $row) {
            /** @var DateTimeInterface $start */
            $start = $row['start'];
            /** @var DateTimeInterface $stop */
            $stop = $row['stop'];
            $showId = $row['show_id'];
            $title = $row['title'];
            $subTitle = $row['sub_title'];
            $description = $row['description'];
            $channelName = $row['name'];
            $channelId = $row['channel_id'];;

            if ($previousChannelName !== $channelName) {
                $result[$channelId] = [];
                $result[$channelId]['channelName'] = $channelName;
            }

            if (!$this->isSameDay($previousStartTime, $start)) {
                $result[$channelId][$start->format(self::DAY_OF_YEAR)] = [];
            }

            $entry = [];
            $entry['start'] = $start->getTimestamp();
            $entry['stop'] = $stop->getTimestamp();
            $entry['id'] = $showId;
            $entry['title'] = $title;
            $entry['subTitle'] = $subTitle;
            $entry['description'] = $description;

            $result[$channelId][$start->format(self::DAY_OF_YEAR)][] = $entry;

            $previousChannelName = $channelName;
            $previousStartTime = $start;
        }

        return $result;
    }

    private function isSameDay(DateTimeInterface $firstTimeStamp = null, DateTimeInterface $secondTimeStamp = null) {
        if (!is_null($firstTimeStamp) && !is_null($secondTimeStamp)) {
            return $firstTimeStamp->format(self::DAY_OF_YEAR) === $secondTimeStamp->format(self::DAY_OF_YEAR);
        } else {
            return $firstTimeStamp === $secondTimeStamp;
        }
    }

}