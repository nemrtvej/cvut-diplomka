<?php

namespace AppBundle\Model\TvSchedule\Import\Container;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Channel
{
    /**
     * @var string
     */
    private $channelName;

    /**
     * @var string
     */
    private $xmlId;

    /**
     * @var Collection|Show[]
     */
    private $shows;

    /**
     * @param $channelName
     * @param $xmlId
     */
    public function __construct($channelName, $xmlId)
    {
        $this->channelName = $channelName;
        $this->xmlId = $xmlId;
        $this->shows = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getChannelName()
    {
        return $this->channelName;
    }

    /**
     * @return mixed
     */
    public function getXmlId()
    {
        return $this->xmlId;
    }

    /**
     * @return mixed
     */
    public function getShows()
    {
        return $this->shows;
    }

    public function addShow(Show $show)
    {
        $this->shows->add($show);
    }
}