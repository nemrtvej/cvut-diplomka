<?php

namespace AppBundle\Model\TvSchedule\Import\Container;

use AppBundle\Entity\Show as ShowEntity;
use Doctrine\Common\Collections\Collection;

class CollisionContainer
{
    /**
     * @var Show
     */
    private $insertedShow;

    /**
     * @var Collection|ShowEntity[]
     */
    private $existingShows;

    /**
     * CollisionContainer constructor.
     *
     * @param Show                    $insertedShow
     * @param Collection|ShowEntity[] $existingShows
     */
    public function __construct(Show $insertedShow, Collection $existingShows)
    {
        $this->insertedShow = $insertedShow;
        $this->existingShows = $existingShows;
    }

    /**
     * @return Show
     */
    public function getInsertedShow()
    {
        return $this->insertedShow;
    }

    /**
     * @return Collection|ShowEntity[]
     */
    public function getExistingShows()
    {
        return $this->existingShows;
    }
}