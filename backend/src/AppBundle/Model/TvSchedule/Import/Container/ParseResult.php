<?php

namespace AppBundle\Model\TvSchedule\Import\Container;

use Doctrine\Common\Collections\Collection;

class ParseResult
{
    /**
     * @var Collection|Channel[]
     */
    private $channels;

    /**
     * @var Collection|Show[]
     */
    private $shows;

    /**
     * Result constructor.
     * @param Channel[]|Collection $channels
     * @param Show[]|Collection $shows
     */
    public function __construct(Collection $channels, Collection $shows)
    {
        $this->channels = $channels;
        $this->shows = $shows;
    }

    /**
     * @return Channel[]|Collection
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @return Show[]|Collection
     */
    public function getShows()
    {
        return $this->shows;
    }
}