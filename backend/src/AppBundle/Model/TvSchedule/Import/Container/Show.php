<?php

namespace AppBundle\Model\TvSchedule\Import\Container;

class Show
{
    const DATE_FORMAT = 'YmdHis T';
    /**
     * @var Channel
     */
    private $channel;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $stop;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $subTitle;

    /**
     * @var string
     */
    private $desc;

    /**
     * Show constructor.
     * @param Channel $channel
     * @param \DateTime $start
     * @param \DateTime $stop
     * @param string $name
     * @param string $subTitle
     * @param string $desc
     */
    public function __construct(Channel $channel, \DateTime $start, \DateTime $stop, $name, $subTitle, $desc)
    {
        $this->channel = $channel;
        $this->start = $start;
        $this->stop = $stop;
        $this->name = $name;
        $this->subTitle = $subTitle;
        $this->desc = $desc;

        $this->channel->addShow($this);
    }

    /**
     * @return Channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @return \DateTime
     */
    public function getStop()
    {
        return $this->stop;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    public function __toString()
    {
        return sprintf(
            '%s (%s): %s - %s',
            $this->getName(),
            $this->getChannel()->getChannelName(),
            $this->getStart()->format('H:i:s d.m.Y'),
            $this->getStop()->format('H:i:s d.m.Y')
        );
    }
}