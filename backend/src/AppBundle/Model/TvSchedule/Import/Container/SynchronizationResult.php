<?php

namespace AppBundle\Model\TvSchedule\Import\Container;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Stopwatch\Stopwatch;

class SynchronizationResult
{
    private $channelsInserted;

    private $showsInserted;

    private $showCollisions;

    private $showCollisionsRemoved;

    private $showCollisionsInserted;

    private $stopwatch;

    private $collisions;

    /**
     * SynchronizationResult constructor.
     */
    public function __construct()
    {
        $this->stopwatch = new Stopwatch();
        $this->stopwatch->start('sync');
        $this->channelsInserted = 0;
        $this->showsInserted = 0;
        $this->showCollisions = 0;
        $this->showCollisionsRemoved = 0;
        $this->showCollisionsInserted = 0;
        $this->collisions = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getChannelsInserted()
    {
        return $this->channelsInserted;
    }

    /**
     * @return int
     */
    public function getShowsInserted()
    {
        return $this->showsInserted;
    }

    /**
     * @return int
     */
    public function getShowCollisions()
    {
        return $this->showCollisions;
    }

    /**
     * @return int
     */
    public function getShowCollisionsRemoved()
    {
        return $this->showCollisionsRemoved;
    }

    /**
     * @return int
     */
    public function getShowCollisionsInserted()
    {
        return $this->showCollisionsInserted;
    }


    public function bumpChannelsInserted() {
        $this->channelsInserted++;
    }

    public function bumpShowsInserted() {
        $this->showsInserted++;
    }

    public function bumpShowCollisions() {
        $this->showCollisions++;
    }

    public function bumpShowCollisionsRemoved() {
        $this->showCollisionsRemoved++;
    }

    public function bumpShowCollisionsInserted() {
        $this->showCollisionsInserted++;
    }

    public function getTimeDiff()
    {
        return $this->stopwatch->stop('sync')->getDuration();
    }

    public function addCollision(CollisionContainer $collisionContainer)
    {
        $this->collisions->add($collisionContainer);
    }

    /**
     * @return ArrayCollection
     */
    public function getCollisions()
    {
        return $this->collisions;
    }
}