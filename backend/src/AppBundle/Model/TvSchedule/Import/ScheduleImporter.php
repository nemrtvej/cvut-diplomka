<?php

namespace AppBundle\Model\TvSchedule\Import;

use AppBundle\Model\TvSchedule\Import\Container\CollisionContainer;
use AppBundle\Model\TvSchedule\Import\Container\Show;
use AppBundle\Model\TvSchedule\Import\Container\SynchronizationResult;
use Doctrine\Common\Collections\Collection;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class ScheduleImporter
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ScheduleParser
     */
    private $scheduleParser;

    /**
     * @var ScheduleSynchronizer
     */
    private $scheduleSynchronizer;

    /**
     * ScheduleImporter constructor.
     * @param LoggerInterface $logger
     * @param ScheduleParser $scheduleParser
     * @param ScheduleSynchronizer $scheduleSynchronizer
     */
    public function __construct(
        LoggerInterface $logger,
        ScheduleParser $scheduleParser,
        ScheduleSynchronizer $scheduleSynchronizer
    )
    {
        $this->logger = $logger;
        $this->scheduleParser = $scheduleParser;
        $this->scheduleSynchronizer = $scheduleSynchronizer;
    }

    public function importXmlTv(File $file, OutputInterface $output)
    {
        try {
            $this->logger->debug(sprintf('Parsing file %s', $file->getPathname()));
            $parseResult = $this->scheduleParser->parseXmlTv($file);

            $this->logger->info(sprintf('Parsed %s channels and %s shows', $parseResult->getChannels()->count(), $parseResult->getShows()->count()));
            $this->logger->info('Import started');
            $synchronizationResult = $this->scheduleSynchronizer->synchronize($parseResult->getChannels(), $parseResult->getShows(), $output);

            $this->logger->info(sprintf('Schedule synchronized.'));

            if ($synchronizationResult->getShowCollisions() > 0) {
                $this->printCollisions($synchronizationResult->getCollisions(), $output);
            }
            $this->logger->info($this->convertResultToMessage($synchronizationResult));

            $this->logger->info('TV schedule was imported.');
        } catch (ScheduleImportException $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);

            throw $e;
        }
    }

    private function convertResultToMessage(SynchronizationResult $result)
    {
        return sprintf(
            'Duration:  %s seconds. %s channels inserted. %s shows inserted. %s show collisions (%s removed and %s added).',
            ($result->getTimeDiff() / 1000.0),
            $result->getChannelsInserted(),
            $result->getShowsInserted(),
            $result->getShowCollisions(),
            $result->getShowCollisionsRemoved(),
            $result->getShowCollisionsInserted()
        );
    }

    /**
     * @param Collection|CollisionContainer[] $collisions
     * @param OutputInterface                 $output
     */
    private function printCollisions(Collection $collisions, OutputInterface $output)
    {
        $output->writeln('Following collisions occured: ');

        foreach ($collisions as $collision) {
            $output->writeln(sprintf('New entry: %s', $collision->getInsertedShow()->__toString()));

            foreach ($collision->getExistingShows() as $existingShow) {
                $output->writeln(sprintf('Current entry: %s', $existingShow->__toString()));
            }

            $output->writeln('');
        }

    }

}