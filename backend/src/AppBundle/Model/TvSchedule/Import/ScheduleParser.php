<?php

namespace AppBundle\Model\TvSchedule\Import;

use AppBundle\Model\TvSchedule\Import\Container\Channel;
use AppBundle\Model\TvSchedule\Import\Container\ParseResult;
use AppBundle\Model\TvSchedule\Import\Container\Show;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SimpleXMLElement;
use Symfony\Component\HttpFoundation\File\File;

class ScheduleParser
{
    /**
     * @param File $file
     * @return ParseResult
     * @throws ScheduleImportException
     */
    public function parseXmlTv(File $file)
    {
        $tv = simplexml_load_file($file->getPathname());

        if (is_null($tv)) {
            throw new ScheduleImportException('Given file is not a valid XML');
        }

        $channels = $this->parseChannels($tv);

        $shows = $this->parseShows($channels, $tv);

        return new ParseResult($channels, $shows);
    }

    /**
     * @param \SimpleXMLElement $tv
     * @return Collection
     */
    private function parseChannels(\SimpleXMLElement & $tv)
    {
        $channelMap = new ArrayCollection();

        foreach ($tv->channel as $channel) {
            $channelName = (string) $channel->{'display-name'};
            $xmlId = (string) $channel['id'];

            $channel = new Channel($channelName, $xmlId);
            $channelMap->set($xmlId, $channel);
        }

        return $channelMap;
    }

    /**
     * @param Collection|Channel[] $channels
     * @param SimpleXMLElement $tv
     * @return Collection
     * @throws ScheduleImportException
     */
    private function parseShows($channels, & $tv)
    {
        $showsMap = new ArrayCollection();

        foreach ($tv->programme as $programme) {
            $channelId = (string) $programme['channel'];
            $startString = $programme['start'];
            $stopString = $programme['stop'];
            $name = (string) $programme->title;
            $subTitle = (string) $programme->{'sub-title'};
            $desc = (string) $programme->desc;

            $channel = $channels->get($channelId);

            if (is_null($channel)) {
                throw new ScheduleImportException('XmlTv contains programme for channel which is not defined in this XmlTv');
            }

            $startDateTime = $this->parseDateTimeString($startString);
            $stopDateTime = $this->parseDateTimeString($stopString);

            $show = new Show($channel, $startDateTime, $stopDateTime, $name, $subTitle, $desc);

            $showsMap->add($show);
        }

        return $showsMap;
    }

    /**
     * @param $dateString
     * @return \DateTime
     */
    private function parseDateTimeString($dateString)
    {
        return \DateTime::createFromFormat(Show::DATE_FORMAT, $dateString);
    }
}