<?php

namespace AppBundle\Model\TvSchedule\Import;

use AppBundle\Entity\Channel as ChannelEntity;
use AppBundle\Entity\Show as ShowEntity;
use AppBundle\Model\ChannelModel;
use AppBundle\Model\ShowModel;
use AppBundle\Model\TvSchedule\Import\Container\CollisionContainer;
use AppBundle\Model\TvSchedule\Import\Container\Show as ShowContainer;
use AppBundle\Model\TvSchedule\Import\Container\Channel as ChannelContainer;
use AppBundle\Model\TvSchedule\Import\Container\SynchronizationResult;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;


class ScheduleSynchronizer
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ChannelModel
     */
    private $channelModel;

    /**
     * @var ShowModel
     */
    private $showModel;

    /**
     * ScheduleSynchronizer constructor.
     * @param EntityManager $entityManager
     * @param ChannelModel $channelModel
     * @param ShowModel $showModel
     */
    public function __construct(EntityManager $entityManager, ChannelModel $channelModel, ShowModel $showModel)
    {
        $this->entityManager = $entityManager;
        $this->channelModel = $channelModel;
        $this->showModel = $showModel;
    }

    /**
     * @param Collection|ChannelContainer[] $channels
     * @param Collection|ShowContainer[]    $shows
     * @param OutputInterface        $output
     *
     * @return SynchronizationResult
     * @throws \Exception
     */
    public function synchronize(Collection $channels, Collection $shows, OutputInterface $output)
    {
        try {
            $synchronizationResult = new SynchronizationResult();
            $this->entityManager->beginTransaction();


            $channelsInDb = $this->channelModel->getListOfChannelForSynchronization();

            $this->synchronizeChannels($synchronizationResult, $channels, $channelsInDb, $output);
            $this->synchronizeShows($synchronizationResult, $shows, $channelsInDb, $output);

            $this->entityManager->flush();
            $this->entityManager->commit();

            return $synchronizationResult;
        } catch (\Exception $e) {
            $this->entityManager->rollback();

            throw $e;
        }
    }

    /**
     * @param SynchronizationResult         $result
     * @param Collection|ChannelContainer[] $channels
     * @param Collection|ChannelEntity[]    $channelsInDb
     * @param OutputInterface        $output
     *
     * @return int
     */
    private function synchronizeChannels(SynchronizationResult $result, Collection $channels, Collection $channelsInDb, OutputInterface $output)
    {
        $progress = new ProgressBar($output, $channels->count());
        $progress->start();

        $channels->map(function(ChannelContainer $channel) use ($channelsInDb, $result, $progress) {

            $progress->advance();
            if ($channelsInDb->containsKey($channel->getXmlId())) {
                return;
            }
            $result->bumpChannelsInserted();
            $createdChannel = $this->channelModel->createChannel($channel->getXmlId(), $channel->getChannelName(), $autoFlush = false);
            $channelsInDb->set($createdChannel->getXmlId(), $createdChannel);
        });

        $progress->finish();
    }

    /**
     * @param SynchronizationResult      $result
     * @param Collection|ShowContainer[] $shows
     * @param Collection|ChannelEntity[] $channelsInDb
     * @param OutputInterface     $output
     */
    private function synchronizeShows(SynchronizationResult $result, Collection $shows, Collection $channelsInDb, OutputInterface $output)
    {
        $progress = new ProgressBar($output, $shows->count());
        $progress->start();
        $progress->setRedrawFrequency(500);

        $shows->map(
            function(ShowContainer $show) use ($result, $channelsInDb, $progress) {
                $progress->advance();

                $showChannel = $channelsInDb->get($show->getChannel()->getXmlId());
                // $existingShows = $this->showModel->findShowInSlot($show->getChannel()->getXmlId(), $show->getStart(), $show->getStop());
                $existingShows = $this->showModel->findShows($show->getChannel()->getXmlId(), $show->getName(), $show->getStart(), $show->getStop());

                if (count($existingShows) === 0) {
                    $this->showModel->createShowFromContainer($showChannel, $show);
                    $result->bumpShowsInserted();
                }
            }
        );

        $progress->finish();
    }

    private function isSame(ShowEntity $showEntity, ShowContainer $showContainer)
    {
        return
            $showEntity->getChannel()->getXmlId() === $showContainer->getChannel()->getXmlId() &&
            $showEntity->getTitle() === $showContainer->getName();
    }

    private function resolveCollision(SynchronizationResult $result, ChannelEntity $channel, ShowContainer $show, ArrayCollection $existingShows)
    {
        $result->bumpShowCollisions();
        $collision = new CollisionContainer($show, $existingShows);
        $result->addCollision($collision);

        $existingShows->filter(function(ShowEntity $existingShow) use ($show) {
            $overlapBetweenExistingStartAndNewEnd = abs($existingShow->getStart()->getTimestamp() - $show->getStop()->getTimestamp());
            $overlapBetweenExistingStopAndNewStart = abs($existingShow->getStop()->getTimestamp() - $show->getStart()->getTimestamp());

            $thresholdSeconds = 5*60;

            return !($overlapBetweenExistingStartAndNewEnd < $thresholdSeconds || $overlapBetweenExistingStopAndNewStart < $thresholdSeconds);
        })->map(function(ShowEntity $show) use ($result) {
            $this->showModel->removeShow($show);
            $result->bumpShowCollisionsRemoved();
        });

        $this->showModel->createShowFromContainer($channel, $show);
        $result->bumpShowCollisionsInserted();
    }
}