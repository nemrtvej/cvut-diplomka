<?php

namespace AppBundle\Model;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Entity\Show;
use AppBundle\Entity\User;
use AppBundle\Entity\UserScheduledRecord;
use AppBundle\Enum\ScheduledRecordStatesEnum;
use AppBundle\Enum\UserScheduledRecordStatesEnum;
use AppBundle\Model\Exception\ScheduledRecordException;
use AppBundle\Model\Exception\UserScheduledRecordException;
use AppBundle\Repository\UserScheduledRecordRepository;
use Doctrine\ORM\NonUniqueResultException;
use Exception;

class UserScheduledRecordModel extends AbstractModel
{

    /**
     * @var ScheduledRecordModel
     */
    private $scheduledRecordModel;

    /**
     * @var UserScheduledRecordRepository
     */
    private $userScheduledRecordRepository;

    /**
     * UserScheduledRecordModel constructor.
     *
     * @param ScheduledRecordModel          $scheduledRecordModel
     * @param UserScheduledRecordRepository $userScheduledRecordRepository
     */
    public function __construct(
        ScheduledRecordModel $scheduledRecordModel,
        UserScheduledRecordRepository $userScheduledRecordRepository
    )
    {
        $this->scheduledRecordModel = $scheduledRecordModel;
        $this->userScheduledRecordRepository = $userScheduledRecordRepository;
    }

    /**
     * @param Show $show
     * @param User $user
     *
     * @return UserScheduledRecord
     * @throws ScheduledRecordException
     * @throws UserScheduledRecordException
     */
    public function scheduleRecordingForUser(Show $show, User $user)
    {
        try {
            /** @var UserScheduledRecord $currentRecording */
            $currentRecording = $this->userScheduledRecordRepository->findScheduledRecording($show, $user);
            if (!is_null($currentRecording)) {
                return $currentRecording;
            }

            $scheduledRecord = $this->scheduledRecordModel->getScheduledRecordForShow($show);
            $userScheduledRecord = new UserScheduledRecord($scheduledRecord, $user);

            $this->entityManager->persist($userScheduledRecord);
            $this->entityManager->flush($userScheduledRecord);

            return $userScheduledRecord;
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            throw new UserScheduledRecordException('Failed to register new recording', null, $e);
        }
    }

    public function unscheduleRecordingForUser($show, $user)
    {
        try {
            $this->entityManager->beginTransaction();
            /** @var UserScheduledRecord $currentUserRecording */
            $currentUserRecording = $this->userScheduledRecordRepository->findScheduledRecording($show, $user);
            if (is_null($currentUserRecording)) {
                return;
            }

            if ($currentUserRecording->getState() !== UserScheduledRecordStatesEnum::PLANNED) {
                throw new UserScheduledRecordException('Show was already recorded, you cannot cancel its recording');
            }

            $scheduledRecording = $currentUserRecording->getScheduledRecord();
            $scheduledRecording->removeUserRecording($currentUserRecording);

            $this->entityManager->remove($currentUserRecording);
            $this->entityManager->flush($currentUserRecording);
            $this->scheduledRecordModel->checkRecordingForRemoval($scheduledRecording);
            $this->entityManager->commit();
        } catch (UserScheduledRecordException $e) {
            $this->entityManager->rollback();
            throw $e;
        } catch (Exception $e) {
            $this->entityManager->rollback();
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            throw new UserScheduledRecordException('Failed to unregister recording', null, $e);
        }
    }

    public function removeRecordedRecord(UserScheduledRecord $userScheduledRecord)
    {
        try {
            $this->entityManager->beginTransaction();

            if ($userScheduledRecord->getScheduledRecord()->getState() !== ScheduledRecordStatesEnum::PROCESSED) {
                throw new UserScheduledRecordException('Show was not processed yet, therefore it cannot be removed');
            }

            $userScheduledRecord->setState(UserScheduledRecordStatesEnum::REMOVED);
            $this->entityManager->flush($userScheduledRecord);
            $this->entityManager->commit();
        } catch (UserScheduledRecordException $e) {
            $this->entityManager->rollback();
            throw $e;
        } catch (Exception $e) {
            $this->entityManager->rollback();
            $this->logger->critical($e->getMessage(), ['exception' => $e]);
            throw new UserScheduledRecordException('Failed to unregister recording', null, $e);
        }

        try {
            $scheduledRecording = $userScheduledRecord->getScheduledRecord();
            $this->scheduledRecordModel->checkProcessedRecordingForRemoval($scheduledRecording);
        } catch (ScheduledRecordException $e) {
            // nop
        }
    }

    /**
     * @param ScheduledRecord $scheduledRecord
     * @param User            $user
     *
     * @return mixed
     * @throws UserScheduledRecordException
     */
    public function findByScheduledRecordAndUser(ScheduledRecord $scheduledRecord, User $user)
    {
        try {
            return $this->userScheduledRecordRepository->findByScheduledRecordAndUser($scheduledRecord, $user);
        } catch (NonUniqueResultException $e) {
            throw new UserScheduledRecordException(
                sprintf(
                    'User %s has not scheduled recording for ScheduledRecord#%s',
                    $user->getEmail(),
                    $scheduledRecord->getId()
                ),
                null,
                $e
            );
        }
    }

    /**
     * @param Show $show
     * @param User $user
     *
     * @return UserScheduledRecord
     * @throws NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function findByShowAndUser(Show $show, User $user)
    {
        return $this->userScheduledRecordRepository->findScheduledRecording($show, $user);
    }
}