<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ChannelRepository extends EntityRepository
{

    public function getListOfChannelForSynchronization()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('CHANNEL');
        $qb->from('AppBundle:Channel', 'CHANNEL', 'CHANNEL.xmlId');

        return $qb->getQuery()->getResult();
    }
}