<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Entity\User;
use AppBundle\Enum\ScheduledRecordStatesEnum;
use Doctrine\ORM\EntityRepository;

class ScheduledRecordRepository extends EntityRepository
{
    public function findRecordsInStatesForUser($states, User $user)
    {
        $qb = $this->createQueryBuilder('SCHEDULED_RECORD');
        $qb->addSelect('SHOW');
        $qb->addSelect('SCHEDULED_RECORD');
        $qb->addSelect('CHANNEL');

        $qb->join('SCHEDULED_RECORD.show', 'SHOW');
        $qb->join('SHOW.channel', 'CHANNEL');
        $qb->join('SCHEDULED_RECORD.userScheduledRecords', 'USER_SCHEDULED_RECORD');

        $qb->andWhere('SCHEDULED_RECORD.state IN (:scheduledStates)');
        $qb->setParameter('scheduledStates', $states);
        $qb->andWhere('USER_SCHEDULED_RECORD.user = :user');
        $qb->setParameter('user', $user);

        $qb->orderBy('SHOW.start', 'ASC');

        return $qb->getQuery()->getResult();
    }
}