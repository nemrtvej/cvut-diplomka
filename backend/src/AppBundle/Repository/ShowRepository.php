<?php

namespace AppBundle\Repository;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use PDO;

class ShowRepository extends EntityRepository
{

    public function findShowsInSlot($channelXmlId, \DateTime $start, \DateTime $stop)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata('AppBundle:Show', 'SHOW', [], $rsm::COLUMN_RENAMING_NONE);

        $sql = '
            SELECT SHOW.*
            FROM shows SHOW
            JOIN channels CHANNEL ON CHANNEL.id = SHOW.channel_id
            WHERE CHANNEL.xml_id = :xmlId
            AND (:start, :stop) OVERLAPS (SHOW.start, SHOW.stop)
        ';

        $nativeQuery = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $nativeQuery->setParameter('start', $start);
        $nativeQuery->setParameter('stop', $stop);
        $nativeQuery->setParameter('xmlId', $channelXmlId);

        return $nativeQuery->getResult();
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     *
     * @return array
     */
    public function findDataForExport(DateTimeInterface $dateFrom, DateTimeInterface $dateTo)
    {
        $sql = '
            SELECT 
                s.start, 
                s.stop, 
                s.id as show_id, 
                s.title, 
                s.sub_title, 
                s.description, 
                c.name, 
                c.id as channel_id
            FROM shows s
            INNER JOIN channels c ON (c.id = s.channel_id)
            WHERE s.start BETWEEN :from AND :to AND c.dvb_source_url IS NOT NULL
            ORDER BY c.id ASC, s.start ASC
        ';

        $rsm = new ResultSetMapping();
        $rsm
            ->addScalarResult('start', 'start', Type::DATETIME)
            ->addScalarResult('stop', 'stop', Type::DATETIME)
            ->addScalarResult('show_id', 'show_id')
            ->addScalarResult('title', 'title')
            ->addScalarResult('sub_title', 'sub_title')
            ->addScalarResult('description', 'description')
            ->addScalarResult('name', 'name')
            ->addScalarResult('channel_id', 'channel_id');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query
            ->setParameter(':from', $dateFrom, Type::DATETIME)
            ->setParameter(':to', $dateTo, Type::DATETIME);

        return $query->getResult($query::HYDRATE_ARRAY);
    }

    public function findShows($channelId, $showTitle, \DateTimeInterface $start, \DateTimeInterface $stop)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata('AppBundle:Show', 'SHOW', [], $rsm::COLUMN_RENAMING_NONE);

        $sql = '
            SELECT SHOW.*
            FROM shows SHOW
            JOIN channels CHANNEL ON CHANNEL.id = SHOW.channel_id
            WHERE CHANNEL.xml_id = :xmlId
            AND SHOW.title = :title
            AND extract(epoch FROM SHOW.start) BETWEEN :startBegin AND :startEnd
            AND extract(epoch FROM SHOW.stop) BETWEEN :stopBegin AND :stopEnd
        ';

        $nativeQuery = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $tolerancy = 5 * 60;
        // because weird timezone & doctrine & sql db...
        $nativeQuery->setParameter('startBegin', $start->getTimestamp() - $tolerancy + $start->getOffset());
        $nativeQuery->setParameter('startEnd', $start->getTimestamp() + $tolerancy + $start->getOffset());
        $nativeQuery->setParameter('stopBegin', $stop->getTimestamp() - $tolerancy + $start->getOffset());
        $nativeQuery->setParameter('stopEnd', $stop->getTimestamp() + $tolerancy + $start->getOffset());
        $nativeQuery->setParameter('xmlId', $channelId);
        $nativeQuery->setParameter('title', $showTitle);

        return $nativeQuery->getResult();
    }
}