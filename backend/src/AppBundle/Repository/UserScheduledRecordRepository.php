<?php

namespace AppBundle\Repository;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Entity\Show;
use AppBundle\Entity\User;
use AppBundle\Enum\UserScheduledRecordStatesEnum;
use Doctrine\ORM\EntityRepository;

class UserScheduledRecordRepository extends EntityRepository
{
    /**
     * @param Show $show
     * @param User $user
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findScheduledRecording(Show $show, User $user)
    {
        $qb = $this->createQueryBuilder('USER_SCHEDULED_RECORDING');
        $qb->join('USER_SCHEDULED_RECORDING.scheduledRecord', 'SCHEDULED_RECORD');

        $qb->andWhere('USER_SCHEDULED_RECORDING.user = :user');
        $qb->andWhere('SCHEDULED_RECORD.show = :show');
        $qb->setParameter('user', $user);
        $qb->setParameter('show', $show);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $scheduledRecordId
     * @param User            $user
     *
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByScheduledRecordAndUser($scheduledRecordId, User $user)
    {
        $qb = $this->createQueryBuilder('USER_SCHEDULED_RECORDING');
        $qb->join('USER_SCHEDULED_RECORDING.scheduledRecord', 'SCHEDULED_RECORD');

        $qb->andWhere('USER_SCHEDULED_RECORDING.user = :user');
        $qb->andWhere('SCHEDULED_RECORD.id = :id');
        $qb->setParameter('user', $user);
        $qb->setParameter('id', $scheduledRecordId);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param ScheduledRecord $scheduledRecord
     *
     * @return mixed
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function countUnremovedUserRecordings(ScheduledRecord $scheduledRecord)
    {
        $qb = $this->createQueryBuilder('USER_SCHEDULED_RECORD');
        $qb->select('COUNT(USER_SCHEDULED_RECORD.id)');
        $qb->andWhere('USER_SCHEDULED_RECORD.scheduledRecord = :scheduledRecordId');
        $qb->andWhere('USER_SCHEDULED_RECORD.state NOT IN (:removedStates)');
        $qb->setParameter('scheduledRecordId', $scheduledRecord);
        $qb->setParameter('removedStates', [UserScheduledRecordStatesEnum::REMOVED]);

        return $qb->getQuery()->getSingleScalarResult();
    }
}