<?php

namespace AppBundle\Service;

use AppBundle\Entity\ScheduledRecord;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class RabbitFacade
{
    /**
     * @var ProducerInterface
     */
    private $removeRecordedRecordingQueue;

    /**
     * @var ProducerInterface
     */
    private $scheduleRecordingQueue;

    /**
     * @var ProducerInterface
     */
    private $startTranscodingQueue;

    /**
     * RabbitFacade constructor.
     *
     * @param ProducerInterface $removeRecordedRecordingQueue
     * @param ProducerInterface $scheduleRecordingQueue
     * @param ProducerInterface $startTranscodingQueue
     */
    public function __construct(
        ProducerInterface $removeRecordedRecordingQueue,
        ProducerInterface $scheduleRecordingQueue,
        ProducerInterface $startTranscodingQueue
    ) {
        $this->removeRecordedRecordingQueue = $removeRecordedRecordingQueue;
        $this->scheduleRecordingQueue = $scheduleRecordingQueue;
        $this->startTranscodingQueue = $startTranscodingQueue;
    }

    public function sendRemoveScheduledRecordingMessage(ScheduledRecord $scheduledRecord)
    {
        $data = $this->convertScheduleRecordToString('queue_remove', $scheduledRecord);
        $message = json_encode($data);

        $this->scheduleRecordingQueue->publish($message);
    }

    public function sendRemoveRecordedRecordingMessage(ScheduledRecord $scheduledRecord)
    {
        $data = [
            'id' => $scheduledRecord->getShow()->getId(),
        ];
        $message = json_encode($data);

        $this->removeRecordedRecordingQueue->publish($message);
    }

    public function sendScheduleRecordingMessage(ScheduledRecord $scheduledRecord)
    {
        $data = $this->convertScheduleRecordToString('queue_insert', $scheduledRecord);
        $message = json_encode($data);

        $this->scheduleRecordingQueue->publish($message);
    }

    public function sendTranscodeMessage(ScheduledRecord $scheduledRecord)
    {
        $data = [
            'id' => $scheduledRecord->getShow()->getId(),
            'action' => 'transcode',
        ];

        $this->startTranscodingQueue->publish(json_encode($data));
    }

    private function convertScheduleRecordToString($action, ScheduledRecord $scheduledRecord)
    {
        return [
            'start' => $scheduledRecord->getShow()->getStart()->getTimestamp(),
            'stop' => $scheduledRecord->getShow()->getStop()->getTimestamp(),
            'source' => $scheduledRecord->getShow()->getChannel()->getDvbSourceUrl(),
            'id' => $scheduledRecord->getShow()->getId(),
            'action' => $action,
        ];
    }
}