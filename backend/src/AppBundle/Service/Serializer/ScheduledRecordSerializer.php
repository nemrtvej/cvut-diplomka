<?php

namespace AppBundle\Service\Serializer;

use AppBundle\Entity\ScheduledRecord;
use Doctrine\Common\Collections\Collection;

class ScheduledRecordSerializer
{
    public function serializeScheduledCollection(Collection $scheduledRecords)
    {
        return $scheduledRecords->map(function(ScheduledRecord $scheduledRecord) {
            return $this->serializeScheduledRecord($scheduledRecord);
        });
    }

    public function serializeScheduledRecord(ScheduledRecord $scheduledRecord)
    {
        return [
            'name' => $scheduledRecord->getShow()->getTitle(),
            'station' => $scheduledRecord->getShow()->getChannel()->getName(),
            'timeFrom' => $scheduledRecord->getShow()->getStart()->getTimestamp(),
            'timeTo' => $scheduledRecord->getShow()->getStop()->getTimestamp(),
            'id' => $scheduledRecord->getShow()->getId(),
            'state' => $scheduledRecord->getState(),
        ];
    }

    public function serializeRecordedRecord(ScheduledRecord $recordedRecord)
    {
        $timeProcessed = is_null($recordedRecord->getTimeProcessed()) ? '': $recordedRecord->getTimeProcessed()->getTimestamp();

        return [
            'name' => $recordedRecord->getShow()->getTitle(),
            'station' => $recordedRecord->getShow()->getChannel()->getName(),
            'size' => $recordedRecord->getSize(),
            'timeProcessed' => $timeProcessed,
            'id' => $recordedRecord->getId(),
            'length' => $recordedRecord->getLength(),
        ];
    }

    public function serializeRecordedCollection(Collection $recordedRecords)
    {
        return $recordedRecords->map(function(ScheduledRecord $recordedRecord) {
            return $this->serializeRecordedRecord($recordedRecord);
        });
    }
}