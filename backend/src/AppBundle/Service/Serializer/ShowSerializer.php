<?php

namespace AppBundle\Service\Serializer;

use AppBundle\Entity\Show;

class ShowSerializer
{
    public function serializeShow(Show $show)
    {
        $data = [
            'stop' => $show->getStop()->getTimestamp(),
            'start' => $show->getStart()->getTimestamp(),
            'subTitle' => $show->getSubTitle(),
            'title' => $show->getTitle(),
            'description' => $show->getDescription(),
        ];
    }
}