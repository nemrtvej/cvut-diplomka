<?php

namespace AppBundle\Service;

use AppBundle\Entity\ScheduledRecord;
use AppBundle\Entity\Show;
use AppBundle\Entity\User;
use AppBundle\Entity\UserScheduledRecord;
use AppBundle\Enum\UserScheduledRecordStatesEnum;
use Doctrine\Common\Collections\ArrayCollection;

class ShowRecordedNotificator
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendShowRecordedMails(ScheduledRecord $record)
    {
        $recipients = new ArrayCollection();

        $userScheduledRecords = $record->getUserScheduledRecords();
        foreach ($userScheduledRecords as $userScheduledRecord) {
            if ($userScheduledRecord->getState() === UserScheduledRecordStatesEnum::PLANNED) {
                $recipients->add($userScheduledRecord->getUser());
            }
        }

        foreach ($recipients as $recipient) {
            $this->sendMessage($recipient, $record->getShow());
        }
    }

    private function sendMessage(User $recipient, Show $show)
    {
        $message = new \Swift_Message('Nahraný pořad');
        $message->setTo($recipient->getEmail());
        $message->setBody($this->twig->render('@App/email/show_recorded.html.twig', ['show' => $show, 'user' => $recipient]));

        $this->mailer->send($message);
    }
}