import pika
import ConfigParser
import json
import time
import shutil
from sender import Sender

config = ConfigParser.RawConfigParser()
config.read('config.ini')

rabbit_host = config.get('RabbitMQ', 'host')
rabbit_port = config.getint('RabbitMQ', 'port')
rabbit_user = config.get('RabbitMQ', 'user')
rabbit_password = config.get('RabbitMQ', 'password')
rabbit_queue = 'remove_recorded_recording'

credentials = pika.PlainCredentials(rabbit_user, rabbit_password)
connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_host, port=rabbit_port, credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue=rabbit_queue, durable=True)

def send_msg(msg):
    sender = Sender('/var/cleaner/config.ini', 'recording_status')
    sender.send(json.dumps(msg))
    sender.close()

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    msg = json.loads(body)
    id = msg.get('id')
    try:
        shutil.rmtree("/data/%s" % int(id))
    except OSError:
        pass
    send_msg({'id': id, 'status': 'REMOVED'})


channel.basic_consume(callback, queue=rabbit_queue, no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()