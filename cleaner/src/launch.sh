#!/bin/sh


echo "Updating requirements"
pip install -r requirements.txt

echo "Launching consumer"
python -u consumer.py > /dev/stdout 2>&1