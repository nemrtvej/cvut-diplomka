<?php
$filename = $_GET['path'];
$filePath = dirname(__FILE__)  . "/" . $filename;
$fileExists = file_exists($filePath);
if (!$fileExists) {
	http_response_code(404);
	die("404 document not found");
}


switch ($_SERVER['REQUEST_METHOD']) {
	case "GET":
	case "POST":
		http_response_code(200);
		echo file_get_contents($filePath);
		break;
	case "DELETE":
	case "PUT":
		http_response_code(204);
		die();
}