/**
 * Class making calls to REST API.
 *
 * @constructor
 */
var APICommunicator = function(serverPath) {
    this._serverPath = serverPath;
};

APICommunicator.prototype = Object.create(AbstractCommunicator.prototype);

/**
 * @public
 *
 * @returns Promise
 */
APICommunicator.prototype.fetchCpuUsageData = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/admin/statistics/cpu";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Fetch whole TV schedule.
 *
 * @public
 *
 * @returns Promise
 */
APICommunicator.prototype.fetchTvProgram = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/schedule/full";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};

/**
 *
 * @public
 *
 * @returns Promise
 */
APICommunicator.prototype.fetchDiskUsageData = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/admin/statistics/disk";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};


/**
 * Fetch data about user statistics
 * @public
 *
 * @return Promise
 */
APICommunicator.prototype.fetchUserStatisticsData = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/admin/statistics/users";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Fetch data about record statistics
 * * @public
 *
 * @return Promise
 */
APICommunicator.prototype.fetchRecordStatisticsData = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/admin/statistics/records";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};


/**
 * Fetch data about user's records.
 *
 * @return Promise
 */
APICommunicator.prototype.fetchUserRecords = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/user/recorded";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Fetch data about user's schedules.
 *
 * @return Promise
 */
APICommunicator.prototype.fetchUserSchedules = function() {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/user/scheduled";
        this.makeGetRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Mark recorded show for deletion.
 *
 * @param id
 * @return Promise
 */
APICommunicator.prototype.deleteRecordedShow = function(id) {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/show/recorded/"+id;
        this.makeDeleteRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Schedule recording of show "id";
 * @param id
 * @returns {*|c}
 */
APICommunicator.prototype.scheduleShow = function(id) {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/show/scheduled/"+id;
        this.makePostRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Unschedule recording of show "id";
 * @param id
 * @returns {*|c}
 */
APICommunicator.prototype.unscheduleShow = function(id) {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/show/scheduled/"+id;
        this.makeDeleteRequest(url, resolve, reject);
    }.bind(this));
};


/**
 * Mark scheduled show for deletion.
 *
 * @param id
 * @return Promise
 */
APICommunicator.prototype.deleteScheduledShow = function(id) {
    return new Promise(function(resolve, reject) {
        var url = this._serverPath + "/show/scheduled/"+id;
        this.makeDeleteRequest(url, resolve, reject);
    }.bind(this));
};

/**
 * Make GET request to given URL.
 * If everything is ok, success function in promise gets the data from page.
 * Else, fail function is called.
 *
 * @param url
 * @param resolve callback
 * @param reject callback
 * @private
 */
APICommunicator.prototype.makeGetRequest = function(url, resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);

    xhr.onload = function (e) {
        if (this.status >= 200 && this.status <= 299) {
            resolve(this.responseText);
        } else if (this.status === 404) {
            reject("API possibly broken: Page not found");
        }
    };

    xhr.onerror = function (e) {
        reject(e);
    };

    xhr.withCredentials = true;
    xhr.send();
};


/**
 * Make DELETE request to given URL.
 * If everything is ok, success function in promise gets the data from page.
 * Else, fail function is called.
 *
 * @param url
 * @param resolve callback
 * @param reject callback
 * @private
 */
APICommunicator.prototype.makeDeleteRequest = function(url, resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('DELETE', url, true);

    xhr.onload = function (e) {
        if (this.status >= 200 && this.status <= 299) {
            resolve(this.responseText);
        } else if (this.status === 404) {
            reject("API possibly broken: Page not found");
        }
    };

    xhr.onerror = function (e) {
        reject(e);
    };
    xhr.withCredentials = true;
    xhr.send();
};


/**
 * Make POST request to given URL.
 * If everything is ok, success function in promise gets the data from page.
 * Else, fail function is called.
 *
 * @param url
 * @param resolve callback
 * @param reject callback
 * @private
 */
APICommunicator.prototype.makePostRequest = function(url, resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);

    xhr.onload = function (e) {
        if (this.status >= 200 && this.status <= 299) {
            resolve(this.responseText);
        } else if (this.status === 404) {
            reject("API possibly broken: Page not found");
        }
    };

    xhr.onerror = function (e) {
        reject(e);
    };
    xhr.withCredentials = true;
    xhr.send();
};