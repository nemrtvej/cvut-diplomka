/**
 * Interface enforcer for APICommunicator, LocalStorageCommunicator and other communicators.
 * @constructor
 */
var AbstractCommunicator = function() {

};

/**
 * Can communicator work?
 * @public
 *
 * @return boolean
 */
AbstractCommunicator.prototype.isOnline = function() {
    throw new NotImplementedException("isOnline() is not implemented.");
};

/**
 * fetch data for disk usage
 * @public
 *
 * @return Promise
 */
AbstractCommunicator.prototype.fetchDiskUsageData = function() {
    throw new NotImplementedException("fetchDiskUsageGraph() is not implemented.");
};

/**
 * fetch data for cpu usage
 * @public
 *
 * @return Promise
 */
AbstractCommunicator.prototype.fetchCpuUsageData = function() {
    throw new NotImplementedException("fetchCpuUsageData() is not implemented.");
};

/**
 * Fetch data about user statistics
 *
 * @public
 *
 * @return Promise
 */
AbstractCommunicator.prototype.fetchUserStatisticsData = function() {
    throw new NotImplementedException("fetchUserStatisticsData() is not implemented.");
};

/**
 * Fetch data about record statistics
 *
 * @return Promise
 */
AbstractCommunicator.prototype.fetchRecordStatisticsData = function() {
    throw new NotImplementedException("fetchRecordStatisticsData() is not implemented.");
};

/**
 * Delete recorded show.
 *
 * @return Promise
 */
AbstractCommunicator.prototype.deleteRecordedShow = function() {
    throw new NotImplementedException("deleteRecordedShow() is not implemented.");
};

/**
 * Delete scheduled show.
 *
 * @return Promise
 */
AbstractCommunicator.prototype.deleteScheduledShow = function() {
    throw new NotImplementedException("deleteScheduledShow() is not implemented.");
};

/**
 * Return whole TV program.
 *
 * @public
 * @return Promise
 */
AbstractCommunicator.prototype.fetchTvProgram = function() {
    throw new NotImplementedException("fetchTvProgram() is not implemented.");
};

/**
 * Unschedule show with id.
 *
 * @public
 * @return Promise
 */
AbstractCommunicator.prototype.unscheduleShow = function(id) {
    throw new NotImplementedException("unscheduleShow() is not yet implemented");
};


/**
 * Schedule show with id.
 *
 * @public
 * @return Promise
 */
AbstractCommunicator.prototype.scheduleShow = function(id) {
    throw new NotImplementedException("scheduleShow() is not yet implemented");
};

/**
 * Fetch data about user's records.
 *
 * @return Promise
 */
AbstractCommunicator.prototype.fetchUserSchedules = function() {
    throw new NotImplementedException("fetchUserSchedules() is not implemented.");
};

/**
 * Fetch data about user's records.
 *
 * @return Promise
 */
AbstractCommunicator.prototype.fetchUserRecords = function() {
    throw new NotImplementedException("fetchUserRecords() is not implemented.");
};

