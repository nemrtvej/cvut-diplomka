/**
 * Class that provides API calls for the rest of application.
 *
 * @constructor
 */
var Communicator = function(apiCommunicator, localStorageCommunicator) {
    this._apiCommunicator = apiCommunicator;
    this._localStorageCommunicator = localStorageCommunicator;

    this._isOnline = navigator.onLine;

    this.bindOnlineStatusWatcher();
};

Communicator.prototype = Object.create(AbstractCommunicator.prototype);


/**
 * @public
 *
 * @returns Promise
 */
Communicator.prototype.fetchCpuUsageData = function() {
    if (this.isOnline()) {
        return this._apiCommunicator.fetchCpuUsageData();
    } else {
        throw new Error("Cannot fetch disk usage while offline.");
    }
};


/**
 * Fetch data about record statistics
 * * @public
 *
 * @return Promise
 */
Communicator.prototype.fetchRecordStatisticsData = function() {
    if (this.isOnline()) {
        return this._apiCommunicator.fetchRecordStatisticsData();
    }  else {
        throw new Error("Cannot fetch record statistics while offline.");
    }
};


/**
 * Fetch data about record statistics
 * * @public
 *
 * @return Promise
 */
Communicator.prototype.fetchUserStatisticsData = function() {
    if (this.isOnline()) {
        return this._apiCommunicator.fetchUserStatisticsData();
    }  else {
        throw new Error("Cannot fetch user statistics while offline.");
    }
};


/**
 * @public
 *
 * @returns Promise
 */
Communicator.prototype.fetchDiskUsageData = function() {
    if (this.isOnline()) {
        return this._apiCommunicator.fetchDiskUsageData();
    } else {
        throw new Error("Cannot fetch disk usage while offline.");
    }
};


/**
 * Fetch data about user's records.
 *
 * @return Promise
 */
Communicator.prototype.fetchUserSchedules = function() {
    if (this.isOnline()) {
        var successCallback = function(resultData) {
            this._localStorageCommunicator.setData(this._localStorageCommunicator.USER_SCHEDULES_KEY, resultData);

            return new Promise(function(resolve, reject) {
                resolve(resultData);
            });
        }.bind(this);

        var failCallback = function() {
            return this._localStorageCommunicator.fetchUserSchedules();
        }.bind(this);

        var promise = this._apiCommunicator.fetchUserSchedules();
        promise.then(successCallback, failCallback);

        return promise;
    } else {
        return this._localStorageCommunicator.fetchUserSchedules();
    }
};


/**
 * Fetch data about user's records.
 *
 * @return Promise
 */
Communicator.prototype.fetchUserRecords = function() {
    if (this.isOnline()) {
        var successCallback = function(resultData) {
            this._localStorageCommunicator.setData(this._localStorageCommunicator.USER_RECORDS_KEY, resultData);

            return new Promise(function(resolve, reject) {
                resolve(resultData);
            });
        }.bind(this);

        var failCallback = function() {
            return this._localStorageCommunicator.fetchUserRecords();
        }.bind(this);

        var promise = this._apiCommunicator.fetchUserRecords();
        promise.then(successCallback, failCallback);

        return promise;
    } else {
        return this._localStorageCommunicator.fetchUserRecords();
    }
};

/**
 *
 * @param id
 * @returns {Promise}
 */
Communicator.prototype.deleteRecordedShow = function(id) {
    if (!this.isOnline()) {
        throw new Error("Cannot call this method when offline.");
    }

    return this._apiCommunicator.deleteRecordedShow(id);
};

/**
 *
 * @param id
 * @returns {Promise}
 */
Communicator.prototype.deleteScheduledShow = function(id) {
    if (!this.isOnline()) {
        throw new Error("Cannot call this method when offline.");
    }

    return this._apiCommunicator.deleteScheduledShow(id);
};

/**
 * Fetch TV schedule
 *
 * @public
 * @returns Promise
 */
Communicator.prototype.fetchTvProgram = function() {
    if (this.isOnline()) {
        var successCallback = function(resultData) {
            this._localStorageCommunicator.setData(this._localStorageCommunicator.TVPROGRAM_KEY, resultData);

            return new Promise(function(resolve, reject) {
                resolve(resultData);
            });
        }.bind(this);

        var failCallback = function() {
            return this._localStorageCommunicator.fetchTvProgram();
        }.bind(this);

        var promise = this._apiCommunicator.fetchTvProgram();
        promise.then(successCallback, failCallback);

        return promise;
    } else {
        return this._localStorageCommunicator.fetchTvProgram();
    }
};

/**
 * Is browser online?
 *
 * @public
 *
 * @returns {boolean|*}
 */
Communicator.prototype.isOnline = function() {
    return this._isOnline;
};

/**
 * @public
 *
 * @param event
 */
Communicator.prototype.updateOnlineStatus = function(event) {
    this._isOnline = navigator.onLine;
};

/**
 * @private
 */
Communicator.prototype.bindOnlineStatusWatcher = function() {
    window.addEventListener('online',  this.updateOnlineStatus.bind(this));
    window.addEventListener('offline', this.updateOnlineStatus.bind(this));
};
