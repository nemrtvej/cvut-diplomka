/**
 * Provides storing and retrieving data to/from localstorage.
 * @constructor
 */
var LocalStorageCommunicator = function() {
    this.USER_RECORDS_KEY = "userRecords";
    this.USER_SCHEDULES_KEY = "userSchedules";
    this.TVPROGRAM_KEY = "tvProgram";

    this.BIGCONTROL_CHANNEL_KEY = "bigControlChannel";
};

LocalStorageCommunicator.prototype = Object.create(AbstractCommunicator.prototype);

/**
 * Returns list of identifiers for channels, which user wants to see.
 *
 * @public
 */
LocalStorageCommunicator.prototype.getEnabledChannels = function() {
    try {
        return this.getData(this.BIGCONTROL_CHANNEL_KEY);
    } catch (error) {
        return null;
    }
};

/**
 * Set list of identifiers for channels, which user wants to see.
 *
 * @public
 */
LocalStorageCommunicator.prototype.setUserSelectedChannels = function(selectedChannels) {
    this.setData(this.BIGCONTROL_CHANNEL_KEY, selectedChannels)
};


/**
 * Returns user recordings stored in LocalStorage
 *
 * @public
 *
 * @return Promise
 */
LocalStorageCommunicator.prototype.fetchUserRecords = function() {
    return new Promise(function(resolve, reject) {
        try {
            var data = this.getData(this.USER_RECORDS_KEY);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    }.bind(this));
};

/**
 * Returns user schedules stored in LocalStorage
 *
 * @public
 *
 * @return Promise
 */
LocalStorageCommunicator.prototype.fetchUserSchedules = function() {
    return new Promise(function(resolve, reject) {
        try {
            var data = this.getData(this.USER_SCHEDULES_KEY);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    }.bind(this));
};

/**
 * @public
 *
 * @returns Promise
 */
LocalStorageCommunicator.prototype.fetchTvProgram = function() {
    return new Promise(function(resolve, reject) {
        try {
            var data = this.getData(this.TVPROGRAM_KEY);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    }.bind(this));
};

/**
 * Set data in localStorage
 *
 * @public
 *
 * @param key
 * @param value
 */
LocalStorageCommunicator.prototype.setData = function(key, value) {
    if (typeof value !== "string") {
        value = JSON.stringify(value);
    }

    localStorage.setItem(key, value);
};

/**
 * Get data from localStorage
 *
 * @private
 */
LocalStorageCommunicator.prototype.getData = function(key) {
    var result = window.localStorage.getItem(key);

    if (result === null) {
        throw new Error("Key '"+key+"' was not found in localStorage");
    }

    return result;
};
