/**
 * Class for setting up the whole DVBGrabber application.
 * @constructor
 */
var DVBGrabber = function() {
    this._router = null;
    this._adminSystemOverview = null;
    this._recordedSubpage = null;
    this._scheduledSubpage = null;
};

/**
 * Trigger this method after DOM is loaded.
 *
 * @public
 */
DVBGrabber.prototype.initialize = function(apiBaseUrl) {
    var apiCommunicator = new APICommunicator(apiBaseUrl);
    var localStorageCommunicator = new LocalStorageCommunicator();
    var communicator = new Communicator(apiCommunicator, localStorageCommunicator);

    var adminSection = document.querySelector("main > section#admin");
    var recordedSection = document.querySelector("main > section#recorded");
    var scheduledSection = document.querySelector("main > section#overview");
    var tvProgramSection = document.querySelector("main > section#channels");

    var svgHelper = new SVGHelper();
    var tableHelper = new TableHelper();
    var calculator  = new Calculator();


    var pieChartRenderer = new PieChart(svgHelper);
    var lineGraphRenderer = new LineGraph(svgHelper);

    var notificationBarElement = document.querySelector("div#notificationBar");
    var notificationHandler = new NotificationHandler(notificationBarElement);

    var programHelper = new ProgramHelper(calculator, apiCommunicator, localStorageCommunicator, notificationHandler);

    this._tvProgramSubpage = new TvProgramSubpage(
        calculator,
        communicator,
        tvProgramSection,
        notificationHandler,
        programHelper
    );

    this._recordedSubpage = new RecordedSubpage(
        calculator,
        communicator,
        recordedSection,
        notificationHandler,
        tableHelper,
        apiBaseUrl
    );

    this._scheduledSubpage = new ScheduledSubpage(
        calculator,
        communicator,
        scheduledSection,
        notificationHandler,
        tableHelper,
        programHelper
    );

    this._adminSystemOverview = new AdminSystemOverview(
        calculator,
        communicator,
        notificationHandler,
        adminSection,
        pieChartRenderer,
        lineGraphRenderer
    );

    var urlClassMap = {
        "admin": this._adminSystemOverview,
        "recorded": this._recordedSubpage,
        "overview": this._scheduledSubpage,
        "channels": this._tvProgramSubpage
    };

    var navbarInstance = $('header > nav');

    this._router = new Routing(urlClassMap, "main > section{hashId}", "active", "channels", navbarInstance);
    this._router.activate();
};