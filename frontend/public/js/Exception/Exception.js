/**
 * Global Exception class.
 *
 * @param string
 * @constructor
 */
var Exception = function(string) {
    this._errorString = string;
};

Exception.prototype = Object.create(Error.prototype);

/**
 * Get exception string.
 *
 * @public
 *
 * @returns {string}
 */
Exception.prototype.getString = function() {
    return this._errorString;
};