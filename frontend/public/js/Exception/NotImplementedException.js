/**
 * Exception thrown when something that was not yet implemented is called.
 *
 * @constructor
 */
var NotImplementedException = function(string) {
    Exception.call(this, string);
};


NotImplementedException.prototype = Object.create(Exception.prototype);
