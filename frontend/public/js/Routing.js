/**
 * Simple routing class.
 *
 * @param urlClassMap
 * @param querySelectorPattern pattern which will querySelector be onto. {hashId} will be replaced
 *        by first part of hash prepended with #.
 * @param activeClassName
 * @param defaultId
 * @param $navbarInstance
 * @constructor
 */
var Routing = function(urlClassMap, querySelectorPattern, activeClassName, defaultId, $navbarInstance) {
    this._urlClassMap = urlClassMap;
    this._querySelectorPattern = querySelectorPattern;
    this._activeClassName = activeClassName;
    this._defaultId = defaultId;
    this._activeSectionElement = null;
    this._activeSectionHandler = null;
    this._$navbarInstance = $navbarInstance;
};

/**
 * Method, that activates routing mechanism.
 *
 * @public
 */
Routing.prototype.activate = function () {
    window.addEventListener("hashchange", this.onHashChange.bind(this));

    this.redraw();
};

/**
 * Callback called by hashchange window event.
 *
 * @param hashChangeEvent
 * @private
 */
Routing.prototype.onHashChange = function(hashChangeEvent) {
    this.redraw();
};

/**
 * Redraw page content.
 *
 * @private
 */
Routing.prototype.redraw = function() {
    var hashParts = this.parseUrl(window.location.hash);

    var mainSectionId = hashParts[0] != "" ? hashParts[0] : this._defaultId;

    this.activateSection(mainSectionId);

    if (this._activeSectionHandler !== null) {
        this._activeSectionHandler.deactivate();
        this._activeSectionHandler = null;

    }

    if (mainSectionId in this._urlClassMap) {
        this._urlClassMap[mainSectionId].activate(hashParts.slice(1));
        this._activeSectionHandler = this._urlClassMap[mainSectionId];
    }

    this._$navbarInstance.find('.' + this._activeClassName).removeClass(this._activeClassName);
    this._$navbarInstance.find('a[href="#'+mainSectionId+'"]').parent('li').addClass(this._activeClassName);
};

/**
 * Activate section with given ID.
 *
 * @param sectionId
 * @private
 */
Routing.prototype.activateSection = function(sectionId) {
    if (this._activeSectionElement != null) {
        this._activeSectionElement.classList.remove(this._activeClassName);
    }

    var selector = this._querySelectorPattern.replace("{hashId}", "#"+sectionId);
    var element = document.querySelector(selector);
    if (element == null) {
        window.location.hash = "";
        return;
    }
    element.classList.add(this._activeClassName);
    this._activeSectionElement = element;
};

/**
 * thx https://gist.github.com/jlong/2428561
 *
 * Returns array of fragments in url hash.
 * Those fragments were separated by /
 *
 * @param url
 * @returns {Array}
 * @private
 */
Routing.prototype.parseUrl = function(url) {
    var parser = document.createElement('a');
    parser.href = url;

    return parser.hash.substr(1).split("/");
};