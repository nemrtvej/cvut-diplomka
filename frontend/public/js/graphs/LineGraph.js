/**
 * Class for creating line graphs.
 *
 * @param svgHelper
 *
 * @constructor
 */
var LineGraph = function(svgHelper) {
    this._svgHelper = svgHelper;
    this._innerGraphCrop = null;
};

/**
 * Renders line graph into given svgElement.
 *
 * @public
 * @param svgElement element to append graph in
 * @param data values to be plotted
 * @param color color of line
 * @param range array of two values - [minimum possible, maximum possible]
 */
LineGraph.prototype.appendGraph = function(svgElement, data, color, range) {
    while (svgElement.firstChild) {
        svgElement.removeChild(svgElement.firstChild);
    }

    var distanceX = 15;
    this._innerGraphCrop = 20;


    var svgAreaHeight = svgElement.viewBox.baseVal.height;
    var svgAreaWidth = svgElement.viewBox.baseVal.width;

    var graphHeight = svgAreaHeight - this._innerGraphCrop;
    var graphWidth = svgAreaWidth - this._innerGraphCrop;

    var offsetX = 5;

    var distanceY = graphHeight / Math.abs(range[1] - range[0]);

    var pointCoordinates = this.computePointCoordinates(
        data,
        distanceX,
        distanceY,
        Math.min(range[0], range[1]),
        Math.max(range[0], range[1]),
        graphHeight,
        5,
        5
    );


    var groupElement = this._svgHelper.createSvgTag("g");
    this.createPercentageLabels(groupElement, graphHeight, graphWidth, distanceY, offsetX);
    this.renderGraph(groupElement, pointCoordinates, color);;
    svgElement.appendChild(groupElement);
};

/**
 * Returns array of coordinates for all points. Every entry contains a tuple of x and y value for point with index i.
 *
 * @private
 * @param values
 * @param distanceX
 * @param distanceY
 * @param minValue
 * @param maxValue
 * @param graphHeight
 * @param offsetX
 */
LineGraph.prototype.computePointCoordinates = function(
    values,
    distanceX,
    distanceY,
    minValue,
    maxValue,
    graphHeight,
    offsetX
) {
    var coordinates = [];

    for (var i = 0; i < values.length; i++) {
        var x = i * distanceX;
        var y = this.computePointValueY(values[i], minValue, maxValue, graphHeight);

        coordinates.push([x+offsetX, y]);

    }

    return coordinates;
};

/**
 * Computes Y position of point with given value.
 * @private
 *
 * @param value
 * @param minValue
 * @param maxValue
 * @param totalHeight
 * @returns {number}
 */
LineGraph.prototype.computePointValueY = function(value, minValue, maxValue, totalHeight) {
    var recomputedValue = value - minValue;
    var steps = totalHeight/maxValue;

    return totalHeight - (recomputedValue * steps) + (this._innerGraphCrop/2);
};

/**
 * Renders line graph into given svgElement.
 *
 * @private
 * @param svgElement
 * @param points
 * @param color
 */
LineGraph.prototype.renderGraph = function(svgElement, points, color) {
    var command = "";

    for (var i = 0; i < points.length; i++) {
        var beggining = (i == 0) ? " M" : " L";
        command += beggining+points[i][0]+","+points[i][1];


        var point = this._svgHelper.createSvgTag("circle", {cx: points[i][0], cy: points[i][1], r: 3, stroke: color});
        svgElement.appendChild(point);
    }
    var path = this._svgHelper.createSvgTag("path", {d: command, fill: "none", stroke: color});
    path.width = 10;

    svgElement.appendChild(path);
};

/**
 * Renders labels for Y axis
 *
 * @param svgElement
 * @param min
 * @param max
 * @param steps
 * @param graphHeight
 * @param graphWidth
 * @param offsetX
 * @param offsetY
 *
 * @private
 */
LineGraph.prototype.createPercentageLabels = function(svgElement, graphHeight, graphWidth, distanceY, offsetX) {
    var color = "rgba(230, 230, 230, 1)";

    var step = 10;

    for (var i = 0; i <= 10; i++) {
        var x1 = offsetX;
        var x2 = graphWidth - offsetX;
        var yCoord = this.computePointValueY(step*i, 0, 100, graphHeight);
        var y1 = yCoord;
        var y2 = yCoord;

        if (y1 % 2 == 0 ) {
            y1 += 1;
            y2 += 1;
        }

        var command = "M"+x1+","+y1;
        command += " L"+x2+","+y2;
        var path = this._svgHelper.createSvgTag("path", {d: command, fill: "none", stroke: color});
        path.style.strokeWidth = 2;
        svgElement.appendChild(path);

        var text = this._svgHelper.createSvgTag("text", { x: x2, y: y2+5, fill: "#000"});

        text.innerHTML = 10*i + " %";
        svgElement.appendChild(text);
    }
};
