/**
 * Class for creating pieChart graph
 * @param svgHelper
 * @constructor
 */
var PieChart = function(svgHelper) {
    this._svgHelper = svgHelper;
};

/**
 * Renders pie style chart into given svgElement using given data and colors.
 *
 * @public
 * @param svgElement
 * @param data
 * @param colors
 */
PieChart.prototype.appendGraph = function(svgElement, data, colors) {
    while (svgElement.firstChild) {
        svgElement.removeChild(svgElement.firstChild);
    }


    if (data.length != colors.length) {
        alert("Error: Data and colors array length mismatch!");
        return;
    }

    var angles = this.computeAngles(data);


    var groupElement = this._svgHelper.createSvgTag("g");
    this.drawArcs(groupElement, angles, data, colors);
    svgElement.appendChild(groupElement);
};

/**
 * Return array of angles for given data.
 *
 * @param data
 *
 * @private
 */
PieChart.prototype.computeAngles = function(data) {
    var total = data.reduce(function(previousValue, currentValue) {
        return previousValue + currentValue;
    });

    var angles = [];

    for(var i = 0; i < data.length; i++){
        var angle = Math.ceil(360 * data[i]/total);
        angles.push(angle);
    }

    return angles;
};

/**
 * Render data into element.
 *
 * @private
 * @param svgElement
 * @param angles
 * @param data
 * @param colors
 * @param width
 * @param height
 */
PieChart.prototype.drawArcs = function(svgElement, angles, data, colors) {
    var arcRadius = 195;
    var pieRadius = 200;

    var startAngle = 0;
    var endAngle = 0;
    var x1, x2, y1, y2 = 0;

    for (var i = 0; i < angles.length; i++){
        if (angles[i] < 0.0001) {
            // skip this, if the angle is too small to be rendered.
            continue;
        }

        startAngle = endAngle;
        endAngle = (startAngle + angles[i]);

        x1 = parseInt(Math.round(pieRadius + arcRadius*Math.cos(Math.PI*startAngle/180)));
        y1 = parseInt(Math.round(200 + arcRadius*Math.sin(Math.PI*startAngle/180)));

        x2 = parseInt(Math.round(pieRadius + arcRadius*Math.cos(Math.PI*endAngle/180)));
        y2 = parseInt(Math.round(pieRadius + arcRadius*Math.sin(Math.PI*endAngle/180)));

        var largeArcFlag = ((angles[i] >= 180) ? 1 : 0);
        var sweepFlag = 1;

        var d = this.createPathCommand(x1, x2, y1, y2, pieRadius, arcRadius, largeArcFlag, sweepFlag);
        var arc = this._svgHelper.createSvgTag("path", {d: d, fill: colors[i]});

        svgElement.appendChild(arc);

        arc.onclick = (function (originalData) {
            return function(event) {
                alert("Associated pie piece data: " + originalData);
            }
        })(data[i]);
    }
};

/**
 * Create command for SVG path.
 *
 * @param x1 X coord of start point
 * @param x2 X coord of end point
 * @param y1 Y coord of start point
 * @param y2 Y coord of end point
 * @param pieRadius Radius of whole pie
 * @param arcRadius Radius of the round arc
 * @param largeArcFlag
 * @param sweepFlag
 * @returns {string}
 */
PieChart.prototype.createPathCommand = function(x1, x2, y1, y2, pieRadius, arcRadius, largeArcFlag, sweepFlag) {
    var command = "M"+pieRadius+","+pieRadius; // move to middle

    // If this segment starts in same point, as it ends, we need to create full circle.
    // But SVG does not create full circle from arc paths.
    // Hence, let's create circle.
    // more info: http://stackoverflow.com/questions/5737975/circle-drawing-with-svgs-arc-path
    if (x1 == x2 && y1 == y2) {
        command += " m -"+arcRadius+", 0";
        command += " a "+arcRadius+","+arcRadius+" 0 1,0 "+(arcRadius*2)+",0";
        command += " a "+arcRadius+","+arcRadius+" 0 1,0 -"+(arcRadius*2)+",0";
    } else {
        command += " L" + x1 + "," + y1; // go to first point of arc
        command += " A" + arcRadius +"," + arcRadius; // Arc radius
        command += " 0 " + largeArcFlag + "," + sweepFlag; // Arc flags
        command += " " + x2 + "," + y2 + " z"; // close the arc to second point
    }

    return command;
};