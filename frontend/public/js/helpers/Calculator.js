/**
 * Various calculations
 *
 * @constructor
 */
var Calculator = function() {

};

/**
 * Recalculate number of bytes to human readable string.
 *
 * @param numberOfBytes
 * @private
 */
Calculator.prototype.bytesToString = function(numberOfBytes) {
    var units = ["B", "KiB", "MiB", "GiB", "TiB", "PiB"];
    var order = 0;

    while (numberOfBytes > 1024) {
        numberOfBytes = numberOfBytes / 1024.0;
        order++;

        if (order == units.length-1) {
            break;
        }
    }

    return numberOfBytes.toFixed(2) + " " + units[order];
};

/**
 * Transform to number of seconds to hours:minutes:minutes format
 *
 * @public
 *
 * @param numberOfSeconds
 */
Calculator.prototype.secondsToHumanTime = function(numberOfSeconds) {
//    numberOfSeconds += 7200;

    var hours = parseInt( numberOfSeconds / 3600 ) % 24;
    var minutes = parseInt( numberOfSeconds / 60 ) % 60;
    var seconds = numberOfSeconds % 60;

    return (hours < 10 ? "0" + hours : hours) + ":" +
        (minutes < 10 ? "0" + minutes : minutes) + ":" +
        (seconds  < 10 ? "0" + seconds : seconds);
};

/**
 * Transform to number of seconds to hours:minutes format
 *
 * @public
 *
 * @param numberOfSeconds
 */
Calculator.prototype.timestampToHoursMinutes = function(timestamp) {
    return moment(timestamp * 1000).format('HH:mm');
};


/**
 * @public
 *
 * @param timestamp
 */
Calculator.prototype.timestampToHumanDateTime = function(timestamp) {
    return moment(timestamp * 1000).format('HH:mm:s DD.MM.Y');

    timestamp += 7200;

    var date = new Date(timestamp*1000);
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    var hour = date.getHours();
    var minute = date.getMinutes();
    var seconds = date.getSeconds();

    return hour + ":" + minute + ":" + seconds + " " + day + "." + month + ". " + year;
};

/**
 * @public
 *
 * @param timestamp
 */
Calculator.prototype.timestampToHumanDate = function(timestamp) {
    return moment(timestamp*1000).format('DD.MM.Y');

    var date = new Date(timestamp*1000);
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();

    return day + "." + month + ". " + year;
};

/**
 * Returns Date representing given day in given year.
 *
 * @public
 * @param dayNum
 * @param year
 * @returns {Date}
 *
 * http://stackoverflow.com/a/26185557
 */
Calculator.prototype.getDateFromDayNum = function(dayNum, year){
    var date = new Date();
    if(year){
        date.setFullYear(year);
    }
    date.setMonth(0);
    date.setDate(0);
    var timeOfFirst = date.getTime(); // this is the time in milliseconds of 1/1/YYYY
    var dayMilli = 1000 * 60 * 60 * 24;
    var dayNumMilli = dayNum * dayMilli;
    date.setTime(timeOfFirst + dayNumMilli);
    return date;
};