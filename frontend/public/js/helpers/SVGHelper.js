/**
 * Class containing helpers for SVG
 */
var SVGHelper = function() {

};

/**
 * Create new SVG tag with given attributes.
 *
 * @param tag name of tag
 * @param attributes attributes of tag
 * @private
 */
SVGHelper.prototype.createSvgTag = function(tag, attributes) {
    var element = document.createElementNS('http://www.w3.org/2000/svg', tag);
    for (var k in attributes)
        if (attributes.hasOwnProperty(k)) {
            element.setAttribute(k, attributes[k]);
        }
    return element;
};
