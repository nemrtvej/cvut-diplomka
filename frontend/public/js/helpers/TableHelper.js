/**
 * Helper for creating HTML tables.
 *
 * @constructor
 */
var TableHelper = function() {
};

/**
 * Create new simple table.
 *
 * @param headMap list containing touples [keyOfField, humanReadableNameOfField]
 * @param data list containing objects whose key names are specified in headMap
 * @return {Element}
 * @public
 */
TableHelper.prototype.createTable = function(headMap, data) {
    var table = document.createElement("table");
    table.classList.add('table');
    table.classList.add('table-striped');
    table.classList.add('table-hover');

    var thead = document.createElement("thead");
    var tbody = document.createElement("tbody");

    var tr = document.createElement("tr");
    for (var i = 0; i < headMap.length; i++) {
        var th = document.createElement("th");
        th.innerHTML = headMap[i][1];
        tr.appendChild(th);
    }
    thead.appendChild(tr);


    for (var i = 0; i < data.length; i++) {
        var tr = document.createElement("tr");
        for (var j = 0; j < headMap.length; j++) {
            var td = document.createElement("td");
            td.innerHTML = data[i][headMap[j][0]];
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }

    table.appendChild(thead);
    table.appendChild(tbody);

    return table;
};