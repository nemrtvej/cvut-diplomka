
if (String.webalize === undefined) {
    // source: https://gist.github.com/Majkl578/947036
    String.prototype.webalize = function () {
        var str = this;
        var charlist;
        charlist = [
            ['Á','A'], ['Ä','A'], ['Č','C'], ['Ç','C'], ['Ď','D'], ['É','E'], ['Ě','E'],
            ['Ë','E'], ['Í','I'], ['Ň','N'], ['Ó','O'], ['Ö','O'], ['Ř','R'], ['Š','S'],
            ['Ť','T'], ['Ú','U'], ['Ů','U'], ['Ü','U'], ['Ý','Y'], ['Ž','Z'], ['á','a'],
            ['ä','a'], ['č','c'], ['ç','c'], ['ď','d'], ['é','e'], ['ě','e'], ['ë','e'],
            ['í','i'], ['ň','n'], ['ó','o'], ['ö','o'], ['ř','r'], ['š','s'], ['ť','t'],
            ['ú','u'], ['ů','u'], ['ü','u'], ['ý','y'], ['ž','z']
        ];
        for (var i in charlist) {
            var re = new RegExp(charlist[i][0],'g');
            str = str.replace(re, charlist[i][1]);
        }

        str = str.replace(/[^a-z0-9]\(\)/ig, '-');
        str = str.replace(/\-+/g, '-');
        if (str[0] == '-') {
            str = str.substring(1, str.length);
        }
        if (str[str.length - 1] == '-') {
            str = str.substring(0, str.length - 1);
        }

        return str;
    };
}

if (Date.getDayOfYear === undefined) {
    // http://javascript.about.com/library/bldayyear.htm
    Date.prototype.getDayOfYear = function() {
        var oneJan = new Date(this.getFullYear(),0,1);
        return Math.ceil((this - oneJan) / 86400000);
    }
}
