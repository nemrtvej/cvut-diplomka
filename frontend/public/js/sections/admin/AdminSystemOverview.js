/**
 * Class controlling over Administration => System Overview
 *
 * @constructor
 */
var AdminSystemOverview = function(
    calculator,
    communicator,
    notificationHandler,
    sectionElement,
    pieChartRenderer,
    lineGraphRenderer
) {
    this._calculator = calculator;
    this._isSectionActive = false;
    this._cpuUsageDataRetrieveInterval = 2000;
    this._cpuUsageHistory = 25;
    this._cpuUsageData = null;
    this._communicator = communicator;
    this._notificationHandler = notificationHandler;
    this._pieChartRenderer = pieChartRenderer;
    this._lineGraphRenderer = lineGraphRenderer;
    this._sectionElement = sectionElement;
    this._cpuGraphTimerId = null;
};

/**
 * Call this method when admin-page is displayed.
 *
 * @public
 */
AdminSystemOverview.prototype.activate = function() {
    this._cpuUsageData = [];

    if (!this._communicator.isOnline()) {
        this._notificationHandler.error("Jste offline. Funkčnost administrace je vypnuta.");
        return;
    }

    this._isSectionActive = true;
    this.prepareCpuUsageGraph();
    this.prepareDiskUsageGraph();
    this.prepareStatistics();
};

/**
 * Method called when window is deactivated.
 *
 * @public
 */
AdminSystemOverview.prototype.deactivate = function() {
    this._isSectionActive = false;
    if (this._cpuGraphTimerId !== null) {
        clearTimeout(this._cpuGraphTimerId);
    }
};

/**
 * Render disk-usage graph.
 *
 * @public
 */
AdminSystemOverview.prototype.renderDiskUsageGraph = function(dataString) {
    var parsedData = JSON.parse(dataString);

    var data = [
        parsedData.full,
        parsedData.free
    ];

    var colors = [
        "#f00",
        "#0f0"
    ];

    var labels = [
        "Obsazeno",
        "Volno"
    ];

    var graph = this._sectionElement.querySelector("svg#diskSpaceGraphImage");

    this._pieChartRenderer.appendGraph(graph, data, colors);

    var labelElement = this._sectionElement.querySelector("svg#diskSpaceGraphImage + aside");
    this.renderDiskUsageLabels(labelElement, data, colors, labels);
};


/**
 * Render cpu-usage graph.
 *
 * @public
 */
AdminSystemOverview.prototype.renderCpuUsageGraph = function(responseData) {
    var parsedData = JSON.parse(responseData);

    this._cpuUsageData.push(parsedData.currentPercentLoad);

    this._cpuUsageData = this._cpuUsageData.slice(this._cpuUsageHistory * -1);

    var color = "#00f";
    var range = [0, 100];

    var graph = this._sectionElement.querySelector("svg#cpuGraphImage");

    this._lineGraphRenderer.appendGraph(graph, this._cpuUsageData, color, range);

    if (this._isSectionActive) {
        this._cpuGraphTimerId = setTimeout(this.prepareCpuUsageGraph.bind(this), this._cpuUsageDataRetrieveInterval);
    }
};


/**
 * @public
 */
AdminSystemOverview.prototype.onCommunicationFail = function(error) {
    console.log(error);

    this._notificationHandler.error("Nastala chyba při komunikaci se serverem. Zkuste to prosím za chvíli.");
};


AdminSystemOverview.prototype.renderRecordStatisticsData = function(responseData) {
    var keyListMap = [
        ["totalNumber", "Celkem nahraných pořadů:"],
        ["currentStored", "Aktuálně uložených:"],
        ["totalRecordSize", "Aktuálně zabráno pořady:"]
    ];

    var element = this._sectionElement.querySelector("#recordStatistics");
    element.innerHTML = "";
    var parsedData = JSON.parse(responseData);
    parsedData.totalRecordSize = this._calculator.bytesToString(parsedData.totalRecordSize);

    var list = this.createDefinitionListFromValues(keyListMap, parsedData);

    element.appendChild(list);
};

AdminSystemOverview.prototype.renderUserStatisticsData = function(responseData) {
    var keyListMap = [
        ["totalNumber", "Celkem uživatelů:"]
    ];

    var element = this._sectionElement.querySelector("#userStatistics");
    element.innerHTML = "";
    var parsedData = JSON.parse(responseData);

    var list = this.createDefinitionListFromValues(keyListMap, parsedData);

    element.appendChild(list);
};

/**
 * Create definition list using keys and labels from keyValueMap and values
 * @private
 *
 * @param keyValueMap
 * @param values
 * @returns {Element}
 */
AdminSystemOverview.prototype.createDefinitionListFromValues = function(keyValueMap, values) {

    var list = document.createElement("dl");

    for (var i = 0; i < keyValueMap.length; i++) {
        var name = document.createElement("dt");
        name.innerHTML = keyValueMap[i][1];
        var value = document.createElement("dd");
        value.innerHTML = values[keyValueMap[i][0]];

        list.appendChild(name);
        list.appendChild(value);
    }

    return list;
};

/**
 * Add human-readable legend for graph of disk usage.
 *
 * @param element
 * @param data
 * @param colors
 * @param labels
 *
 * @private
 */
AdminSystemOverview.prototype.renderDiskUsageLabels = function (element, data, colors, labels) {
    element.innerHTML = "";
    var ul = document.createElement("ul");

    var percentages = this.computePercentages(data);

    for (var i = 0; i < data.length; i++) {
        var li = document.createElement("li");
        var box = document.createElement("span");
        box.classList.add("graphLabelBox");
        box.style.backgroundColor = colors[i];

        var label = document.createElement("span");
        label.innerHTML = labels[i] + " " + this._calculator.bytesToString(data[i]) + " ("+percentages[i]+" %)";
        li.appendChild(box);
        li.appendChild(label);
        ul.appendChild(li);
    }

    element.appendChild(ul);
};

/**
 * @private
 */
AdminSystemOverview.prototype.prepareStatistics = function() {
    this._communicator.fetchUserStatisticsData().then(
        this.renderUserStatisticsData.bind(this),
        this.onCommunicationFail.bind(this)
    );
    this._communicator.fetchRecordStatisticsData().then(
        this.renderRecordStatisticsData.bind(this),
        this.onCommunicationFail.bind(this)
    );
};

/**
 *
 * @private
 */
AdminSystemOverview.prototype.prepareDiskUsageGraph = function() {
    this._communicator.fetchDiskUsageData().then(
        this.renderDiskUsageGraph.bind(this),
        this.onCommunicationFail.bind(this)
    );
};

/**
 *
 * @private
 */
AdminSystemOverview.prototype.prepareCpuUsageGraph = function() {
    this._communicator.fetchCpuUsageData().then(
        this.renderCpuUsageGraph.bind(this),
        this.onCommunicationFail.bind(this)
    );
};

/**
 * Returns array of percentages the given data represents.
 *
 * @param data
 * @private
 */
AdminSystemOverview.prototype.computePercentages = function(data) {
    var total = data.reduce(function(previousValue, currentValue) {
        return previousValue + currentValue;
    });

    var percentages = [];

    for (i = 0; i < data.length; i++) {
        percentages.push((data[i] / total).toFixed(2) * 100);
    }

    return percentages;
};

