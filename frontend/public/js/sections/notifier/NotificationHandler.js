/**
 * This class presents nofitication info to user.
 *
 * @constructor
 */
var NotificationHandler = function(element) {
    this._element = element;
    this._defaultTimeout = 10000;
};


/**
 * Display success in notification bar.
 *
 * @param message
 */
NotificationHandler.prototype.success = function(message) {
    this.display(message, "alert-success", 5000);
};

/**
 * Display info in notification bar.
 *
 * @param message
 */
NotificationHandler.prototype.info = function(message) {
    this.display(message, "alert-info", 5000);
};

/**
 * Display error in notification bar.
 *
 * @param message
 */
NotificationHandler.prototype.error = function(message) {
    this.display(message, "alert-danger", 10000);
};

/**
 * Display notification bar with given message
 *
 * @public
 *
 * @param message
 * @param typeClass
 * @param timeout
 *
 */
NotificationHandler.prototype.display = function(message, typeClass, timeout) {
    this._element.classList.remove("hidden");
    this._element.classList.add(typeClass);
    this.scheduleHide(timeout);
    this._element.innerHTML = message;
};

/**
 * Schedule hide of notification bar in timeout milliseconds.
 *
 * @private
 * @param timeout
 *
 */
NotificationHandler.prototype.scheduleHide = function(timeout) {
    if (timeout < 0) {
        timeout = this._defaultTimeout;
    }

    setTimeout(this.hide.bind(this), timeout);
};

/**
 * Hide notification bar.
 *
 * @private
 */
NotificationHandler.prototype.hide = function() {
    this._element.classList.add("hidden");
    this._element.classList.remove("alert-success");
    this._element.classList.remove("alert-info");
    this._element.classList.remove("alert-danger");
};