/**
 *
 * @param apiCommunicator
 * @param localStorageCommunicator
 * @param notificationHandler
 * @constructor
 */
var ProgramHelper = function(calculator, apiCommunicator, localStorageCommunicator, notificationHandler) {
    this._calculator = calculator;
    this._localStorageCommunicator = localStorageCommunicator;
    this._apiCommunicator = apiCommunicator;
    this._notificationHandler = notificationHandler;

    this._programData = null;
    this._freshScheduleNumberOfProgramDays = 8; // Fresh schedule contains eight days to future.
    this.CHANNEL_NAME_KEY = "channelName";
    this._scheduledShows = [];
};


/**
 * Warms-up list of scheduled shows.
 *
 * @param showId
 * @public
 */
ProgramHelper.prototype.initializeScheduledShows = function() {
    this._scheduledShows = [];
    return new Promise(function(resolve, reject) {
        var onFetch = function(responseString) {
            var data = JSON.parse(responseString);
            data.map(function(entry) {
                this._scheduledShows.push(parseInt(entry.id));
            }.bind(this));
            resolve();
        };

        this._apiCommunicator.fetchUserSchedules().then(
            onFetch.bind(this),
            function() { reject(); }
        );
    }.bind(this));
};

/**
 * Returns true if given show is scheduled for recording.
 *
 * @param showId
 * @public
 */
ProgramHelper.prototype.isShowScheduled = function(showId) {
    showId = parseInt(showId);
    return this._scheduledShows.indexOf(showId) > -1;
};

/**
 * Schedule recording of show with id showId
 * @param showId
 * @public
 */
ProgramHelper.prototype.scheduleShow = function(showId) {
    showId = parseInt(showId);

    return new Promise(function(resolve, reject) {
        if (!this.isShowScheduled(showId)) {
            this._apiCommunicator.scheduleShow(showId).then(
                function() {
                    this._scheduledShows.push(showId);
                    resolve();
                }.bind(this),
                reject
            );
        } else {
            resolve();
        }
    }.bind(this));
};

/**
 * Unschedule recording of show with id showId
 * @param showId
 * @public
 */
ProgramHelper.prototype.unscheduleShow = function(showId) {
    showId = parseInt(showId);

    return new Promise(function(resolve, reject) {
        if (this.isShowScheduled(showId)) {
            this._apiCommunicator.unscheduleShow(showId).then(
                function () {
                    this.removeScheduledShowFromCache(showId);
                    resolve();
                }.bind(this),
                reject
            );
        } else {
            resolve();
        }
    }.bind(this));
};

/**
 * Return scheduled show from internal cache.
 * @public
 * @param showId
 */
ProgramHelper.prototype.removeScheduledShowFromCache = function(showId) {
    var index = this._scheduledShows.indexOf(showId);
    this._scheduledShows.splice(index, 1);
};

/**
 * Call to initialize TV schedule data
 *
 * @public
 *
 * @return Promise
 */
ProgramHelper.prototype.initialize = function() {
    var promise = new Promise(function(resolve, reject) {

        var successCallback = function(data) {
            this._localStorageCommunicator.setData(this._localStorageCommunicator.TVPROGRAM_KEY, data);
            this._programData = JSON.parse(data);
            this.ensureProgramFreshness().then(
                function() {
                    resolve();
                },
                null
            );

            this.initializeScheduledShows();
        }.bind(this);

        var apiCommunicatorFailCallback = function(error) {
            reject("Nepodařilo se získat data ze serveru. Zkuste to prosím později.");
        }.bind(this);

        var localStorageFailCallback = function(error) {
            this._apiCommunicator.fetchTvProgram().then(successCallback, apiCommunicatorFailCallback);
        }.bind(this);


        if (this._programData === null) {
            this._localStorageCommunicator.fetchTvProgram().then(successCallback, localStorageFailCallback);
        } else {
            resolve();
        }
    }.bind(this));

    return promise;
};

/**
 * @public
 * @param selectedChannelList
 */
ProgramHelper.prototype.setUserSelectedChannels = function(selectedChannelList) {
    var data = JSON.stringify(selectedChannelList);
    this._localStorageCommunicator.setUserSelectedChannels(data);
};

/**
 * @public
 * @param channels
 */
ProgramHelper.prototype.setUserSelectedChannels = function(channels) {
    this._localStorageCommunicator.setUserSelectedChannels(channels);
};

/**
 * @public
 * @return array
 */
ProgramHelper.prototype.getUserSelectedChannels = function() {
    var data = JSON.parse(this._localStorageCommunicator.getEnabledChannels());
    if (data === 0) {
        data.push(395); // PRIMA ZOOM
        data.push(424); // PRIMA COOL
        data.push(411); // PRIMA
        data.push(434); // FANDA
        data.push(421); // NOVA
        data.push(400); // CT1 HD
        data.push(417); // CT 2
    }

    return data;
};

/**
 * Get list of touples of all channels and their identifiers. First item is channelName, second is identifier.
 *
 * @public
 */
ProgramHelper.prototype.getChannels = function() {
    if (this._programData === null) {
        throw new Error("Call initialize() first!");
    }

    var channels = [];

    for (var key in this._programData) {
        if (this._programData.hasOwnProperty(key)) {
            channels.push([this._programData[key][this.CHANNEL_NAME_KEY], key]);
        }
    }

    return channels;
};

/**
 * Returns dates the program can serve.
 * Returned value is list of tuples, where first item is Date instance, second is identifier (day of year).
 *
 * @public
 * @return list
 */
ProgramHelper.prototype.getDateRange = function() {
    var channels = this.getChannels();
    var channelId = channels[0][1];

    var channelSchedule = this._programData[channelId];
    var keys = [];
    for (var key in channelSchedule) {
        if (channelSchedule.hasOwnProperty(key) && key != this.CHANNEL_NAME_KEY) {
            keys.push(key);
        }
    }

    var result = [];
    var year = (new Date()).getFullYear();
    var previousDayOfYear = -1;

    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);

    for (var i = 0; i < keys.length; i++) {
        if (i >= this._freshScheduleNumberOfProgramDays) {
            break;
        }
        var currentDayOfYear = keys[i];
        if (previousDayOfYear > currentDayOfYear) {
            // because of the end of the year, where 365 is followed by 1
            year++;
        }

        var date = this._calculator.getDateFromDayNum(currentDayOfYear, year);
        if (date >= today) {
            result.push([date, currentDayOfYear]);
        }
        previousDayOfYear = currentDayOfYear;
    }

    return result;
};

/**
 * Return list of shows that given day is on given channel.
 * @param channelId
 * @param dayId
 * @return list
 * @public
 */
ProgramHelper.prototype.getScheduleForChannelAndDay = function(channelId, dayId) {
    var channelData = this._programData[channelId];
    if (channelData === undefined) {
        throw new Error("Wrong channelId ("+channelId+") supplied!");
    }

    var dayData = channelData[dayId-1];
    if (dayData === undefined) {
        return [];
    }

    return dayData;
};

/**
 * Returns name of channel with given id. Throws error when name does not exists.
 *
 * @public
 * @param channelId
 */
ProgramHelper.prototype.getChannelNameById = function(channelId) {
    if (channelId in this._programData) {
        return this._programData[channelId][this.CHANNEL_NAME_KEY];
    } else {
        throw new Error("Wrong channelId supplied ("+channelId+")");
    }
};

/**
 * If stored program is way too stale, try to make update from server.
 *
 * @private
 */
ProgramHelper.prototype.ensureProgramFreshness = function() {
    return new Promise(function(resolve, reject){
        if(!this.isProgramFresh()) {
            var successCallback = function(data) {
                this._programData = JSON.parse(data);
                resolve();
            }.bind(this);

            var apiCommunicatorFailCallback = function(error) {
                reject("Nepodařilo se získat data ze serveru. Zkuste to prosím později.");
            }.bind(this);

            this._apiCommunicator.fetchTvProgram().then(successCallback, apiCommunicatorFailCallback);
        } else {
            resolve();
        }
    }.bind(this));
};

/**
 * Check whether current schedule should be updated.
 *
 * @private
 *
 * @returns {boolean}
 */
ProgramHelper.prototype.isProgramFresh = function() {
    var today = new Date();
    var dayOfYear = today.getDayOfYear();

    var channels = this.getChannels();
    var firstChannelId = channels[0][1];

    var channelSchedule = this._programData[firstChannelId];

    for (var i = 0; i < this._freshScheduleNumberOfProgramDays; i++) {
        if (channelSchedule[dayOfYear+i] === undefined) {
            break;
        }
    }

    return i == this._freshScheduleNumberOfProgramDays;
};
