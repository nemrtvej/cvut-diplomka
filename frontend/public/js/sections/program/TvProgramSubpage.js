/**
 * Class for handling Tv program subpage
 * @constructor
 */
var TvProgramSubpage = function(calculator, communicator, sectionElement, notificationHandler, programHelper) {
    this._calculator = calculator;
    this._communicator = communicator;
    this._sectionElement = sectionElement;
    this._notificationHandler = notificationHandler;
    this._programHelper = programHelper;

    this._controlsInitialized = false;
    this._smallDateControl = sectionElement.querySelector("#smallDateSelect");
    this._bigChannelControl = sectionElement.querySelector("#bigChannelControls");
    this._smallChannelControl = sectionElement.querySelector("#smallChannelSelect");
    this._bigDateControl = sectionElement.querySelector("#bigDateSelect");
    this._bigContainer = sectionElement.querySelector("#bigContainer tr");
    this._itemWidth = 310;

    this._selectedDate = null;
    this._selectedChannel = null;
    this._selectedChannels = [];
    this.CLASS_SCHEDULED = "scheduled";
};

/**
 * Function called when TvProgram section becomes active
 *
 * @public
 */
TvProgramSubpage.prototype.activate = function(urlParts) {
    var failCallback = function(error) {
        this._notificationHandler.error(
            "Nepodařilo se získat přehled pořadů. Zkuste prosím navštívit stránku později."
        );
    }.bind(this);

    if (urlParts.length == 1) {
        this.setSelectedDate(urlParts[0]);
    } else if (urlParts.length == 2) {
        this.setSelectedDate(urlParts[0]);
        this.setSelectedChannel(urlParts[1]);
    }


    var helperInitializedCallback = function() {
        if (!this._controlsInitialized) {
            this.initializeControls();
        }
        this.renderSchedule();
    }.bind(this);

    this._programHelper.initialize().then(helperInitializedCallback, failCallback);
};

/**
 * Function called when TvProgram section becomes unactive
 *
 * @public
 */
TvProgramSubpage.prototype.deactivate = function() {
    //
};

/**
 * Render TV schedule.
 *
 * @public
 */
TvProgramSubpage.prototype.renderSchedule = function() {
    this.renderSmallSchedule();
    this.renderBigSchedule();
};

/**
 * @private
 */
TvProgramSubpage.prototype.checkSelectedCheckboxes = function() {
    var channelIds = this._selectedChannels || [];
    for (var i = 0; i < channelIds.length; i++) {
        var id = channelIds[i];

        var checkbox = this._bigChannelControl.querySelector("input[value='"+id+"']") || {};
        checkbox.checked = true;
    }
};

/**
 * @private
 */
TvProgramSubpage.prototype.renderBigSchedule = function() {
    var container = this._bigContainer;
    container.innerHTML = "";
    var channelIds = this._selectedChannels || [];
    var now = new Date();
    for (var i = 0; i <channelIds.length; i++) {
        var id = channelIds[i];

        var td = document.createElement("td");
        td.classList.add("item");

        var channelName = document.createElement("h3");
        channelName.innerHTML = this._programHelper.getChannelNameById(id);
        td.appendChild(channelName);

        var schedule = this._programHelper.getScheduleForChannelAndDay(id, this._selectedDate);
        if (schedule.length == 0) {
            td.appendChild(this.createNoContentElement());
        }

        for (var j = 0; j < schedule.length; j++) {
            var startTime = (new Date(schedule[j].start * 1000));
            if (startTime >= now) {
                var entry = this.createProgramEntry(schedule[j]);
                td.appendChild(entry);
            }
        }

        container.appendChild(td);
    }
};

/**
 * @private
 * @returns {Element}
 */
TvProgramSubpage.prototype.createNoContentElement = function() {
    var div = document.createElement("div");
    div.innerHTML = "Pro tento den nemáme k této stanici žádná data.";

    return div;
};

/**
 * @private
 */
TvProgramSubpage.prototype.renderSmallSchedule = function() {
    var area = this._sectionElement.querySelector("#smallContainer .item");
    area.innerHTML = "";
    var data = this._programHelper.getScheduleForChannelAndDay(this._selectedChannel, this._selectedDate);
    var now = new Date();

    if (data.length === 0) {
        area.appendChild(this.createNoContentElement());
        return;
    }

    for (var i = 0; i < data.length; i++) {
        var startTime = (new Date(data[i].start * 1000));
        if (startTime >= now) {
            var entry = this.createProgramEntry(data[i]);
            area.appendChild(entry);
        }
    }
};

/**
 * @private
 * @param entry
 * @returns {Element}
 */
TvProgramSubpage.prototype.createProgramEntry = function(entry) {
    var createMicrodataTag = function(element, elementValue, microdataName) {
        var element = document.createElement(element);
        if (microdataName !== null) {
            element.itemProp = microdataName;
        }

        element.innerHTML = elementValue;
        return element;
    };

    var container = document.createElement("div");
    container.dataset.id = entry.id;
    container.itemScope = "";
    container.itemType = "https://schema.org/BroadcastEvent";

    var startElement = createMicrodataTag("span", this._calculator.timestampToHoursMinutes(entry.start), null);
    startElement.classList.add('startTime');
    var nameElement = createMicrodataTag("span", entry.title, "name");
    nameElement.classList.add('name');
    var descriptionElement = createMicrodataTag("span", entry.subTitle, "description");
    descriptionElement.classList.add("description");

    var startTime = new Date(entry.start);
    var stopTime = new Date(entry.stop);

    var additionalInformation = document.createElement("div");
    additionalInformation.classList.add("additionalInformation");

    additionalInformation.appendChild(createMicrodataTag("span", startTime.toISOString(), "startDate"));
    additionalInformation.appendChild(createMicrodataTag("span", stopTime.toISOString(), "stopDate"));

    container.appendChild(startElement);
    container.appendChild(nameElement);
    container.appendChild(descriptionElement);
    container.appendChild(additionalInformation);

    if (this._programHelper.isShowScheduled(entry.id)) {
        container.classList.add(this.CLASS_SCHEDULED);
    }

    container.addEventListener("click", this.onProgramEntryClick.bind(this));

    return container;
};

TvProgramSubpage.prototype.onProgramEntryClick = function(event) {
    var scheduleSuccessCallback = function() {
        this._notificationHandler.success("Ok, tohle bude nahráno.");
    };

    var scheduleFailCallback = function() {
        this._notificationHandler.error("Nahrávání se nepovedlo naplánovat. Zkuste to později.");
    };

    var unscheduleSuccessCallback = function() {
        this._notificationHandler.success("Ok, nebudeme to nahrávat.");
    };

    var unscheduleFailCallback = function() {
        this._notificationHandler.error("Nepodařilo se zrušit naplánované nahrávání. Zkuste to později.");
    };

    var element = event.srcElement || event.target;
    while (element.dataset.id === undefined) {
        element = element.parentNode;
    }
    var id = element.dataset.id;

    if (this._programHelper.isShowScheduled(id)) {
        element.classList.remove(this.CLASS_SCHEDULED);
        var promise = this._programHelper.unscheduleShow(id);
        promise.then(
            unscheduleSuccessCallback.bind(this),
            unscheduleFailCallback.bind(this)
        );
    } else {
        element.classList.add(this.CLASS_SCHEDULED);
        var promise = this._programHelper.scheduleShow(id);
        promise.then(
            scheduleSuccessCallback.bind(this),
            scheduleFailCallback.bind(this)
        );
    }
};

/**
 * @private
 * @param selectedDate
 */
TvProgramSubpage.prototype.setSelectedDate = function(selectedDate) {
    this._selectedDate = selectedDate;
    if (this._bigDateControl.value != selectedDate) {
        this._bigDateControl.value = selectedDate;
    }
    if (this._smallDateControl.value != selectedDate) {
        this._smallDateControl.value = selectedDate;
    }
};

/**
 * @private
 * @param selectedChannel
 */
TvProgramSubpage.prototype.setSelectedChannel = function(selectedChannel) {
    this._selectedChannel = selectedChannel;
};

/**
 * @private
 * @param selectedChannel
 */
TvProgramSubpage.prototype.setSelectedChannels = function(selectedChannels) {
    this._selectedChannels = selectedChannels;
    var numberOfSelectedChannels = (selectedChannels !== null)  ? selectedChannels.length : 0;
    this._bigContainer.style.width = this._itemWidth * Math.max(numberOfSelectedChannels, 2) + "px";
};


/**
 * @private
 */
TvProgramSubpage.prototype.initializeControls = function() {
    this.setSelectedChannels(this._programHelper.getUserSelectedChannels());

    var onSmallDateChange = function(event) {
        var source = event.target || event.srcElement;
        var value = source.value;
        this.gotoScheduleAtDate(value, this._selectedChannel);
    }.bind(this);

    var onSmallChannelChange = function(event) {
        var source = event.target || event.srcElement;
        var value = source.value;
        this.gotoScheduleAtDate(this._selectedDate, value);
    }.bind(this);

    var onBigDateChange = function(event) {
        var source = event.target || event.srcElement;
        var value = source.value;
        this.gotoScheduleAtDate(value);
    }.bind(this);

    var onBigChannelChange = function(event) {
        var values = [];
        var checkboxes = this._bigChannelControl.querySelectorAll('input[type="checkbox"]:checked');
        for (var i = 0; i < checkboxes.length; i++) {
            values.push(checkboxes[i].value);
        }

        this.setSelectedChannels(values);
        this._programHelper.setUserSelectedChannels(values);
        this.renderSchedule();
    }.bind(this);

    this._smallDateControl.addEventListener("change", onSmallDateChange);
    this._smallChannelControl.addEventListener("change", onSmallChannelChange);
    this._bigDateControl.addEventListener("change", onBigDateChange);

    var channels = this._programHelper.getChannels();
    for (var i = 0; i < channels.length; i++) {
        var label = document.createElement("label");
        var input = document.createElement("input");
        input.type = "checkbox";
        input.value = channels[i][1];

        label.appendChild(input);
        label.innerHTML += channels[i][0];
        this._bigChannelControl.appendChild(label);
        this._bigChannelControl.addEventListener("change", onBigChannelChange);

        var option = document.createElement("option");
        option.innerHTML = channels[i][0];
        option.value = channels[i][1];
        if (this._selectedChannel == option.value) {
            option.selected = "selected";
        }
        this._smallChannelControl.appendChild(option);
    }

    var dayNames = ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"];

    var dates = this._programHelper.getDateRange();

    for (var i = 0; i < dates.length; i++) {
        var smallOption = document.createElement("option");
        var bigOption = document.createElement("option");
        var date = dates[i][0];
        var dayName = dayNames[date.getDay()];
        smallOption.value = dates[i][1];
        bigOption.value = dates[i][1];
        var label = dayName + " " + date.getDate() + "." + (date.getMonth()+1);
        smallOption.innerHTML = label;
        bigOption.innerHTML = label;

        if (this._selectedDate == smallOption.value) {
            smallOption.selected = "selected";
            bigOption.selected = "selected";
        }

        this._smallDateControl.appendChild(smallOption);
        this._bigDateControl.appendChild(bigOption);
    }

    if (this._selectedChannel === null) {
        this.setSelectedChannel(channels[0][1]);
    }

    if (this._selectedDate === null) {
        this.setSelectedDate(dates[0][1]);
    }

    this._controlsInitialized = true;
    this.checkSelectedCheckboxes();
};

/**
 * @private
 * @param date
 * @param channel
 */
TvProgramSubpage.prototype.gotoScheduleAtDate = function(date, channel) {
    var url = "channels";
    url += "/"+date;

    if (channel !== undefined && channel !== null && channel !== "null") {
        url += "/"+channel;
    }

    window.location.hash = url;
};