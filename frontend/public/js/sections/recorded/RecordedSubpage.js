/**
 * Class for manipulating data in "recorded" section
 */
var RecordedSubpage = function(calculator, communicator, sectionElement, notificationHandler, tableHelper, apiBaseUrl) {
    this._calculator = calculator;
    this._communicator = communicator;
    this._sectionElement = sectionElement;
    this._notificationHandler = notificationHandler;
    this._tableHelper = tableHelper;
    this._deleteDownloadedShowsButton = null;
    this._apiBaseUrl = apiBaseUrl;
};

/**
 * Method called when this page is activated.
 *
 * @public
 */
RecordedSubpage.prototype.activate = function() {
    if (!this._communicator.isOnline()) {
        this._notificationHandler.info("Jste offline. Zobrazují se pouze data uložená od poslední návštěvy.");
    }
    this.prepareUserRecords();

    if (this._deleteDownloadedShowsButton === null) {
        this._deleteDownloadedShowsButton = this._sectionElement.querySelector("button");
        this._deleteDownloadedShowsButton.addEventListener(
            "click",
            this.deleteDownloadedShowsButtonClicked.bind(this)
        );
    }
};


/**
 * Method called when window is deactivated.
 *
 * @public
 */
RecordedSubpage.prototype.deactivate = function() {

};

/**
 * @public
 */
RecordedSubpage.prototype.onCommunicationFail = function(error) {
    console.log(error);

    this._notificationHandler.info("Nepodařilo se synchronizovat data se serverem. Zkuste to prosím později.");
};

/**
 * Parses data from API and renders user record datagrid.
 *
 * @public
 * @param responseString
 */
RecordedSubpage.prototype.renderUserRecords = function(responseString) {
    var data = JSON.parse(responseString);

    var headMap = [
        ["mark", ""],
        ["id", "ID"],
        ["name", "Pořad"],
        ["station", "Stanice"],
        ["size", "Velikost"],
        ["timeProcessed", "Datum nahrání"],
        ["length", "Délka"],
        ["download", "Stáhnout"],
    ];

    data = this.transformRecordDataForOutput(data);

    var table = this._tableHelper.createTable(headMap, data);
    var parent = this._sectionElement.querySelector("div");
    parent.innerHTML = "";
    parent.appendChild(table);
};

/**
 *
 *
 * @private
 */
RecordedSubpage.prototype.prepareUserRecords = function() {
    this._communicator.fetchUserRecords().then(
        this.renderUserRecords.bind(this),
        this.onCommunicationFail.bind(this)
    );
};

/**
 * Transform data from API to human-readable form.
 *
 * @private
 * @param recordData
 */
RecordedSubpage.prototype.transformRecordDataForOutput = function(recordData) {
    var output = recordData.map(function(entry) {
        var downloadName = entry.name.webalize() + ".mp4";

        entry.size = this._calculator.bytesToString(entry.size);
        entry.length = this._calculator.secondsToHumanTime(entry.length);
        entry.timeProcessed = this._calculator.timestampToHumanDateTime(entry.timeProcessed);

        entry.mark = '<input type="checkbox" name="record[]" value="'+entry.id+'">';
        entry.download = '<a download="'+downloadName+'" href="'+this._apiBaseUrl+'/show/'+entry.id+'">Stáhnout</a>';

        return entry;
    }.bind(this));

    return output;
};

RecordedSubpage.prototype.deleteDownloadedShowsButtonClicked = function(event) {
    event.preventDefault();
    if (!this._communicator.isOnline()) {
        this._notificationHandler.error("V offline režimu nelze mazat nahrané pořady.");
        return;
    }

    var checkboxes = this._sectionElement.querySelectorAll("form input[type=checkbox]:checked");


    var successCallback = function(checkbox) {
        var tr = checkbox.parentNode.parentNode;
        var trParent = tr.parentNode;
        trParent.removeChild(tr);
    };

    for (var i = 0; i < checkboxes.length; i++) {
        var checkbox = checkboxes[i];
        var id = checkbox.value;


        this._communicator.deleteRecordedShow(id).then(
            successCallback.bind(this, checkbox),
            this.onCommunicationFail.bind(this)
        );
    }
};