/**
 * Class for controlling scheduled subpage.
 *
 * @param calculator
 * @param communicator
 * @param sectionElement
 * @param notificationHandler
 * @param tableHelper
 * @constructor
 */
var ScheduledSubpage = function(calculator, communicator, sectionElement, notificationHandler, tableHelper, programHelper) {
    this._calculator = calculator;
    this._communicator = communicator;
    this._sectionElement = sectionElement;
    this._notificationHandler = notificationHandler;
    this._tableHelper = tableHelper;
    this._deleteScheduledShowsButton = null;
    this._programHelper = programHelper;
};

/**
 * Activate this subpage
 *
 * @public
 */
ScheduledSubpage.prototype.activate = function() {
    if (!this._communicator.isOnline()) {
        this._notificationHandler.info("Jste offline. Zobrazují se pouze data uložená od poslední návštěvy.");
    }
    this.prepareUserSchedules();

    if (this._deleteScheduledShowsButton === null) {
        this._deleteScheduledShowsButton = this._sectionElement.querySelector("button");
        this._deleteScheduledShowsButton.addEventListener(
            "click",
            this.deleteScheduledShowsButtonClicked.bind(this)
        );
    }
};

/**
 * @public
 */
ScheduledSubpage.prototype.onCommunicationFail = function(error) {
    console.log(error);

    this._notificationHandler.info("Nepodařilo se synchronizovat data se serverem. Zkuste to prosím později.");
};

/**
 * deactivate this subpage
 *
 * @public
 */
ScheduledSubpage.prototype.deactivate = function() {

};

/**
 * Parses data from API and renders user record datagrid.
 *
 * @public
 * @param responseString
 */
ScheduledSubpage.prototype.renderUserSchedules = function(responseString) {
    var data = JSON.parse(responseString);

    var headMap = [
        ["mark", ""],
        ["day", "Den"],
        ["timeFrom", "Začátek"],
        ["timeTo", "Konec"],
        ["name", "Pořad"],
        ["station", "Stanice"],
        ["state", "Aktuální stav"]
    ];

    data = this.transformRecordDataForOutput(data);

    var table = this._tableHelper.createTable(headMap, data);
    var parent = this._sectionElement.querySelector("div");
    parent.innerHTML = "";
    parent.appendChild(table);
};


/**
 *
 *
 * @private
 */
ScheduledSubpage.prototype.prepareUserSchedules = function() {
    this._communicator.fetchUserSchedules().then(
        this.renderUserSchedules.bind(this),
        this.onCommunicationFail.bind(this)
    );
};


/**
 * Transform data from API to human-readable form.
 *
 * @private
 * @param recordData
 */
ScheduledSubpage.prototype.transformRecordDataForOutput = function(recordData) {
    var states = {
        "SCHEDULED": "Naplánováno",
        "RECORDED": "Nahráno - čeká na překódování",
        "RECORDING": "Nahrávání",
        "TRANSCODING": "Překódovávání",
        "COPYING": "Kopírování na úložiště"
    };

    var output = recordData.map(function(entry) {
        entry.day = this._calculator.timestampToHumanDate(entry.timeFrom);
        entry.timeFrom = this._calculator.timestampToHoursMinutes(entry.timeFrom);
        entry.timeTo = this._calculator.timestampToHoursMinutes(entry.timeTo);
        entry.state = states[entry.state];

        entry.mark = '<input type="checkbox" name="schedule[]" value="'+entry.id+'">';

        return entry;
    }.bind(this));

    return output;
};

ScheduledSubpage.prototype.deleteScheduledShowsButtonClicked = function(event) {
    event.preventDefault();
    if (!this._communicator.isOnline()) {
        this._notificationHandler.error("V offline režimu nelze mazat nahrané pořady.");
        return;
    }

    var checkboxes = this._sectionElement.querySelectorAll("form input[type=checkbox]:checked");


    var successCallback = function(checkbox) {
        var tr = checkbox.parentNode.parentNode;
        var trParent = tr.parentNode;
        trParent.removeChild(tr);
    };

    for (var i = 0; i < checkboxes.length; i++) {
        var checkbox = checkboxes[i];
        var id = checkbox.value;
        this._programHelper.removeScheduledShowFromCache(id);
        this._communicator.deleteScheduledShow(id).then(
            successCallback.bind(this, checkbox),
            this.onCommunicationFail.bind(this)
        );
    }
};