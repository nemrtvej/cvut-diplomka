#!/usr/local/bin/python

import argparse
import os
import subprocess
import time
import json
from sender import Sender

parser = argparse.ArgumentParser(description='Start recording of movie and schedule recording termination.')
parser.add_argument('id', type=str, help='Recording id')
parser.add_argument('source', type=str, help='Source of recording')
parser.add_argument('duration', type=str, help='Duration of recording')

args = parser.parse_args()

stream_url = args.source
recording_id = args.id
duration = args.duration
output_dir = "/data/%s" % (recording_id)

try:
    os.makedirs(output_dir )
except OSError:
    if not os.path.isdir(output_dir):
        raise

def store_data(filename, data):
    file = open(filename, "a")
    file.write(data)
    file.close()

def send_msg(msg):
    sender = Sender('/var/recorder/config.ini', 'recording_status')
    sender.send(json.dumps(msg))
    sender.close()

output_file = "%s/input.raw" % (output_dir)
store_data(("%s/cmd.txt" % (output_dir)), " ".join(['/var/recorder/helpers/record.sh', stream_url, output_file, duration]))

msgData =  {"id": recording_id, "status": "RECORDING"} # see AppBundle\Enum\ScheduledRecordStatesEnum
send_msg(msgData)
store_data(("%s/rabbit.queue.log" % (output_dir)), json.dumps(msgData))

process = subprocess.Popen(['/var/recorder/helpers/record.sh', stream_url, output_file, duration], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout, stderr = process.communicate()

store_data(("%s/record.stdout.log" % (output_dir)), stdout)
store_data(("%s/record.stderr.log" % (output_dir)), stderr)

size = os.path.getsize(output_file)
length = duration

msgData =  {"id": recording_id, "status": "RECORDED", "size": size, "length": length, "time_processed": time.time()} # see AppBundle\Enum\ScheduledRecordStatesEnum
store_data(("%s/rabbit.queue.log" % (output_dir)), json.dumps(msgData))
send_msg(msgData)