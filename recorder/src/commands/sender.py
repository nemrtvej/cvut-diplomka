import pika
import ConfigParser

class Sender(object):
    def __init__(self, config_file_path, queue_name):
        config = ConfigParser.RawConfigParser()
        config.read(config_file_path)

        rabbit_host = config.get('RabbitMQ', 'host')
        rabbit_port = config.getint('RabbitMQ', 'port')
        rabbit_user = config.get('RabbitMQ', 'user')
        rabbit_password = config.get('RabbitMQ', 'password')
        rabbit_queue = queue_name

        credentials = pika.PlainCredentials(rabbit_user, rabbit_password)
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_host, port=rabbit_port, credentials=credentials))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=rabbit_queue, durable=True)
        self.queue_name = queue_name

    def send(self, message):
        self.channel.basic_publish(exchange='', routing_key=self.queue_name, body=message)

    def close(self):
        self.connection.close()