import pika
import ConfigParser
import json
import time
import subprocess

config = ConfigParser.RawConfigParser()
config.read('config.ini')

rabbit_host = config.get('RabbitMQ', 'host')
rabbit_port = config.getint('RabbitMQ', 'port')
rabbit_user = config.get('RabbitMQ', 'user')
rabbit_password = config.get('RabbitMQ', 'password')
rabbit_queue = 'schedule_recording'

credentials = pika.PlainCredentials(rabbit_user, rabbit_password)
connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_host, port=rabbit_port, credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue=rabbit_queue, durable=True)

def call_at(timestamp, shell_command):
    ts = time.gmtime(timestamp)
    timestring = "%02d:%02d %02d.%02d.%02d" % (ts.tm_hour, ts.tm_min, ts.tm_mday, ts.tm_mon, ts.tm_year)
    print "at " + timestring
    process = subprocess.Popen(['at', timestring], stdin=subprocess.PIPE)
    # poll() can be not-None when the at command was not launched - i.e. time was garbled
    if process.poll() is None:
        process.communicate(input=shell_command)
    if process.poll() is None:
        process.terminate()

def get_atq_jobs():
    job_ids = []
    process = subprocess.Popen(['at', '-l'], stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()
    for line in stdout.split("\n"):
        parts = line.split("\t")
        atd_id = parts[0].strip()
        if atd_id != "":
            job_ids.append(atd_id)
    return job_ids

def get_recording_id(job_id):
    process = subprocess.Popen(['at', '-c', job_id], stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()
    for line in stdout.split("\n"):
        if "#RECORD_START" in line or "#RECORD_STOP" in line:
            parts = line.split("-")
            return parts[1]

# algo: go through all entries, find those who have RECORD_START and RECORD_STOP with given ID and remove those.
def remove_show_recording(show_id):
    atq_job_ids = get_atq_jobs()
    for job_id in atq_job_ids:
        recording_id = get_recording_id(job_id)
        if int(recording_id) == int(show_id):
            process = subprocess.Popen(['atrm', job_id])
            process.terminate()

def schedule_recording(msg):
    start = msg.get('start')
    id = msg.get('id')
    stop = msg.get('stop')
    source = msg.get('source')
    duration = int(stop) - int(start)

    shell_command = '#RECORD_START-%s\n/var/recorder/commands/record.py %s "%s" %s' % (id, id, source, duration)
    call_at(start, shell_command)
    # shell_command = "#RECORD_STOP-%s\nstop recording %s" % (id, source)
    # call_at(stop, shell_command)

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    msg = json.loads(body)
    action = msg.get('action')

    if action == 'queue_insert':
        schedule_recording(msg)
    elif action == 'queue_remove':
        id = msg.get('id')
        remove_show_recording(id)
    else:
        print "Unknown action %s" % (action)

channel.basic_consume(callback, queue=rabbit_queue, no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()