#!/usr/bin/env bash

#SOURCE="http://10.0.42.249:8082/stream.avi"
#OUTPUT="/data/stream.avi"
#DURATION=80
CACHE_SIZE_KB=4096

SOURCE="$1"
OUTPUT="$2"
DURATION="$3"

echo "mplayer -cache ${CACHE_SIZE_KB} \"${SOURCE}\" -dumpstream -dumpfile \"${OUTPUT}\""
mplayer -cache ${CACHE_SIZE_KB} "${SOURCE}" -dumpstream -dumpfile "${OUTPUT}" &
RECORDER_PID=$!
sleep ${DURATION}s
kill ${RECORDER_PID}
