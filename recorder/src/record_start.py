#!/usr/local/bin/python

import argparse
import os
import subprocess
import pika
import ConfigParser



config = ConfigParser.RawConfigParser()
config.read('config.ini')

rabbit_host = config.get('RabbitMQ', 'host')
rabbit_port = config.getint('RabbitMQ', 'port')
rabbit_user = config.get('RabbitMQ', 'user')
rabbit_password = config.get('RabbitMQ', 'password')
rabbit_queue = 'schedule_recording'

credentials = pika.PlainCredentials(rabbit_user, rabbit_password)
connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_host, port=rabbit_port, credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue=rabbit_queue, durable=True)




parser = argparse.ArgumentParser(description='Start recording of movie and schedule recording termination.')
parser.add_argument('duration', type=int, help='Recording id')
parser.add_argument('rec_id', type=int, help='Recording id')
parser.add_argument('url', type=str, help='Stream url to record')

args = parser.parse_args()

my_pid = os.getpid()
stream_url = args.url
recording_id = args.rec_id
duration = args.duration

output_file = "/data/%s.raw.mpg" % (recording_id)
command = "avconv  -t %s -i %s -acodec copy -vcodec copy %s" % (duration, stream_url, output_file)
print command
process = subprocess.Popen(command.split(" "), stdout=subprocess.PIPE)

print "My Pi: %s \nRecordingId: %s\nStream url: %s" % (my_pid, recording_id, stream_url)