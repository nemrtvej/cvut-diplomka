import pika
import ConfigParser
import json
import time
import os
import subprocess
from sender import Sender

config = ConfigParser.RawConfigParser()
config.read('config.ini')

rabbit_host = config.get('RabbitMQ', 'host')
rabbit_port = config.getint('RabbitMQ', 'port')
rabbit_user = config.get('RabbitMQ', 'user')
rabbit_password = config.get('RabbitMQ', 'password')
rabbit_queue = 'start_transcoding'

credentials = pika.PlainCredentials(rabbit_user, rabbit_password)
connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_host, port=rabbit_port, credentials=credentials))
channel = connection.channel()

channel.queue_declare(queue=rabbit_queue, durable=True)


def store_data(filename, data):
    file = open(filename, "a")
    file.write(data)
    file.close()

def send_msg(msg):
    sender = Sender('/var/transcoder/config.ini', 'recording_status')
    sender.send(json.dumps(msg))
    sender.close()

def run_transcode(id):
    output_dir = "/data/%s" % (id)

    input = "%s/input.raw" % output_dir
    output = "%s/output.raw" % output_dir
    send_msg({'id': id, 'status': 'TRANSCODING'})
    process = subprocess.Popen(['/var/transcoder/helpers/transcode.sh', input, output], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    store_data(("%s/transcode.stdout.log" % (output_dir)), stdout)
    store_data(("%s/transcode.stderr.log" % (output_dir)), stderr)
    size = os.path.getsize(output)
    send_msg({'id': id, 'status': 'PROCESSED', 'time_processed': time.time(), 'size': size})

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    msg = json.loads(body)
    action = msg.get('action')

    if action == 'transcode':
        run_transcode(msg.get('id'))
    else:
        print "Unknown action %s" % (action)

channel.basic_consume(callback, queue=rabbit_queue, no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()