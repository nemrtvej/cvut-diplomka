#!/bin/bash

export SOURCE="$1"
export OUTPUT="$2"

OUTPUT_DIR="$( dirname ${OUTPUT} )"
mkdir -p ${OUTPUT_DIR}
TRANSCODE_COMMAND=${OUTPUT_DIR}/transcode.sh


envsubst < /var/transcoder/helpers/transcode_command.sh.tpl > ${TRANSCODE_COMMAND}
cd ${OUTPUT_DIR}
bash ${TRANSCODE_COMMAND}                              